package com.game.animalia.display.screen;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;
import com.game.animalia.Assets;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.animalia.display.object.pet.PetGenerator;
import com.game.animalia.network.GameNetwork;
import com.game.animalia.network.GameRequest;
import com.game.animalia.network.MultiplayerConnection;
import com.game.animalia.network.NetworkData;
import com.game.animalia.network.NetworkInterface;
import com.game.animalia.network.PetData;
import com.game.animalia.network.RequestType;
import com.game.animalia.util.BattleInfo;
import com.game.animalia.util.Core;
import com.game.animalia.util.Player;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.listeners.ActorDragListener;
import com.game.framework.utils.L;

public class ArenaScreen extends AbstractScreen implements NetworkInterface {
	
	private BitmapFont font;
	private DisplayText selected;
	private DisplayObject contentWindow;
	
	public ArenaScreen() {
		
		super();
		
		font = Assets.getInstance().generateFont(20, Assets.getInstance().charSet);
		Assets.getInstance().playMusic("sfx/battle.mp3");
		
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion bg = atlas.findRegion("battlescreenbg");
		DisplayObject bgDisplay = new DisplayObject(bg);
		addActor(bgDisplay);
		
		addMenuWindow();
		addInfoWindow();
		addContentWindow();
		
		initScreen();
	}
	
	private void addContentWindow() {
		contentWindow = createWindow("window6", 270,20, 510, 440);
		addActor(contentWindow);
	}

	private void addInfoWindow() {
		DisplayObject infoWindow = createWindow("window6", 20,20, 230, 200);
		addActor(infoWindow);
		
		final Player player = Core.getInstance().player;
		Pet pet = player.getPet();
		String rankString = BattleInfo.getInstance().getRank(player.getRank());
		DisplayText playerName = createText("Name:\n "+player.getName(), 15,144, font);
		playerName.setPreferedWidth(infoWindow.getWidth() - 30);
		DisplayText playerRank = createText("Rank:\n"+rankString, 15, 92, font);
		playerRank.setPreferedWidth(infoWindow.getWidth() - 30);
		final DisplayText gold = createText("Gold:\n "+player.getGold(), 15, 64, font);
		gold.setPreferedWidth(infoWindow.getWidth() - 30);
		gold.addAction(new Action() {
			
			@Override
			public boolean act(float delta) {
				gold.setText("Gold: "+player.getGold());
				return false;
			}
		});
		
		infoWindow.addActor(gold);
		infoWindow.addActor(playerRank);
		infoWindow.addActor(playerName);
	}

	private void addMenuWindow() {
		DisplayObject menuWindow = createWindow("window6", 20,250, 230, 200);
		addActor(menuWindow);
		
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		final DisplayText battleText = createText("Battle", 15,153, font);
		battleText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				//if(selected == battleText){
					showTournament();
				//} else {
					selected = battleText;
					highLight(list, battleText);
				//}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		final DisplayText pvpText = createText("PvP", 15, 96, font);
		//disableText(pvpText);
		pvpText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				//if(selected == pvpText){
					showPvP();
				//} else {
					selected = pvpText;
					highLight(list, pvpText);
				//}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		final DisplayText exitText = createText("Exit", 15, 39, font);
		exitText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				
				//if(selected == exitText){
					Assets.getInstance().stopMusic();
					switchScreen(new MapScreen());
				//} else {
					selected = exitText;
					highLight(list, exitText);
				//}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		
		menuWindow.addActor(battleText);
		menuWindow.addActor(pvpText);
		menuWindow.addActor(exitText);
		list.add(battleText);
		list.add(pvpText);
		list.add(exitText);
	}
	
	private void showPvP() {
		contentWindow.clearChildren();
		contentWindow.clearActions();
		
		final MultiplayerConnection con = new MultiplayerConnection();
		con.setNetworkInterface(this);
		
		final DisplayText multiplayerText = createText("Multiplayer Connection",15,407, font);
		contentWindow.addActor(multiplayerText);
		
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		
		final DisplayText createText = createText(">Create", 15, 0, font);
		createText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(selected == createText) {
					
					Thread t = new Thread(new Runnable() {
						@Override
						public void run() {
							try{
								GameNetwork.startServer(con);
								NetworkData.getInstance().connection = con;
								NetworkData.getInstance().state = NetworkData.WAITING;
								NetworkData.getInstance().isClient = false;
								NetworkData.getInstance().isServer = true;;
								L.wtf("server created!");
							} catch(IOException e) {
								L.e("error creating server");
								e.printStackTrace();
							}
						}
					});
					t.start();
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		list.add(createText);
		
		final DisplayText joinText = createText(">Join", 15, 0, font);
		joinText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(selected == joinText) {
					NetworkData.getInstance().state = NetworkData.SEARCHING_FOR_SERVER;
					Thread t = new Thread(new Runnable(){
						@Override
						public void run() {
							List<InetAddress> hosts = GameNetwork.discoverHost();
							L.wtf("server running.. "+hosts.size());
							if(hosts.size() > 0) {
								NetworkData.getInstance().hosts = hosts;
								NetworkData.getInstance().state = NetworkData.HOST_LIST;
							} else {
								NetworkData.getInstance().hosts = null;
								NetworkData.getInstance().state = NetworkData.HOST_LIST;
							}
						}
					});
					t.start();
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		list.add(joinText);
		
		for(int i=0;i<list.size();i++){
			final DisplayText display = list.get(i);
			display.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					highLight(list, display);
					selected = display;
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		}
		
		createList(contentWindow, list);
		
		contentWindow.addAction(new Action() {
			@Override
			public boolean act(float delta) {
				if(NetworkData.getInstance().state == NetworkData.getInstance().WAITING) {
					waitForOtherPlayer();
					return true;
				}
				return false;
			}
		});
		contentWindow.addAction(new Action() {
			@Override
			public boolean act(float delta) {
				if(NetworkData.getInstance().state == NetworkData.getInstance().HOST_LIST) {
					showServers(NetworkData.getInstance().hosts);
					return true;
				}
				return false;
			}
		});
		
		contentWindow.addAction(new Action() {
			@Override
			public boolean act(float delta) {
				if(NetworkData.getInstance().state == NetworkData.getInstance().SEARCHING_FOR_SERVER) {
					searchingForServers();
					return true;
				}
				return false;
			}
		});
		
		contentWindow.addAction(new Action() {
			@Override
			public boolean act(float delta) {
				if(NetworkData.getInstance().state == NetworkData.getInstance().SHOW_PLAYERS_DATA) {
					showPlayerInfo();
					return true;
				}
				return false;
			}
		});
	}
	
	private void showPlayerInfo() {
		contentWindow.clearChildren();
		final DisplayText title = createText("Player Match",15,407, font);
		title.setPreferedWidth(contentWindow.getWidth());
		contentWindow.addActor(title);
		title.addAction(new Action() {
			public float time;
			@Override
			public boolean act(float delta) {
				time += delta;
				title.setText("Match Starts in "+(int)(10 -(time)));
				if(time >= 10){
					switchScreen(new BattleScreen());
					return true;
				}
				return false;
			}
		});
		
		showPlayerInfo(Core.getInstance().player,15,250);
		showPlayerInfo(Core.getInstance().foe.get(0),15,25);
	}
	
	private void showPlayerInfo(Player player,float x,float y) {
		Group group = new Group();
		group.setPosition(x, y);
		contentWindow.addActor(group);
		DisplayText playerName = createText(player.getName(), 200, 130, font);
		DisplayText petName = createText(player.getPet().name+" lv"+player.getPet().level, 200, 70, font);
		DisplayText rank = createText(BattleInfo.getInstance().getRank(player.getRank()), 200, 100, font);
		PetDisplay petDisplay = addPetDisplay(player.getPet(), 0, 0);
		
		
		group.addActor(playerName);
		group.addActor(petName);
		group.addActor(rank);
		group.addActor(petDisplay);
	}
	
	private void waitForOtherPlayer() {
		contentWindow.clearChildren();
		DisplayText text = createText("Waiting For Player",15,407, font);
		contentWindow.addActor(text);
	}
	
	private PetDisplay addPetDisplay(Pet pet,float x,float y) {
		PetDisplay petDisplay = new PetDisplay(pet);
		petDisplay.setPosition(x,y);
		petDisplay.addListener(new ActorDragListener());
		return petDisplay;
	}
	
	private void noServerFound() {
		contentWindow.clearChildren();
		DisplayText noServerFoundText = createText("No Server Found",15,407, font);
		contentWindow.addActor(noServerFoundText);
		NetworkData.getInstance().state = NetworkData.getInstance().LOBBY;
	}
	
	private void searchingForServers() {
		contentWindow.clearChildren();
		DisplayText text = createText("Searching for Servers...",15,407, font);
		contentWindow.addActor(text);
	}
	
	private void showServers(List<InetAddress> hosts) {
		if( hosts == null) {	
			noServerFound();
			return;
		}
		
		contentWindow.clearChildren();
		DisplayText tournamentText = createText("Server List",15,407, font);
		contentWindow.addActor(tournamentText);
		
		final MultiplayerConnection con = new MultiplayerConnection();
		con.setNetworkInterface(this);
		
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		for(int i=0;i<hosts.size();i++) {
			final String ip = hosts.get(i).getHostAddress();
			final DisplayText serverText = createText(">"+ip, 15, 0, font);
			list.add(serverText);
			serverText.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					if(selected == serverText) {
						try {
						 GameNetwork.connectTo(ip,con);
						 NetworkData.getInstance().connection = con;
						 NetworkData.getInstance().isClient = true;
						 NetworkData.getInstance().isServer = false;
						 L.wtf("connected to "+ip);
						} catch(IOException e) {
							L.e("connecting to "+ip+" error");
							e.printStackTrace();
						}
					}
					return super.touchDown(event, x, y, pointer, button);
				}
			});
			
		}
		
		for(int i=0;i<list.size();i++){
			final DisplayText display = list.get(i);
			display.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					highLight(list, display);
					selected = display;
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		}
		
		createList(contentWindow, list);
	}
	
	private void showTournament() {
		contentWindow.clearChildren();
		
		DisplayText tournamentText = createText("Tournament",15,407, font);
		contentWindow.addActor(tournamentText);
		
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		
		final DisplayText amateurText = createText(">Amateur", 15, 0, font);
		amateurText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(selected == amateurText) {
					startBattle(1,5,2);
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		list.add(amateurText);
		
		final DisplayText warriorText = createText(">Warrior", 15, 0, font);
		warriorText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(selected == warriorText) {
					startBattle(5,10,3);
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		list.add(warriorText);
		
		final DisplayText championText = createText(">Champion", 15, 0, font);
		championText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(selected == championText) {
					startBattle(10,20,4);
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		list.add(championText);
		
		for(int i=0;i<list.size();i++){
			final DisplayText display = list.get(i);
			display.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					highLight(list, display);
					selected = display;
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		}
		
		createList(contentWindow, list);
		
	}
	
	private void startBattle(int minlevel,int maxLevel,int rankID) {
		
		int rand = (int)(Math.random() * 4) + 1;
		Pet foePet1 = PetGenerator.getInstance().getPet(rand);
		foePet1.setLevel(minlevel);
		
		rand = (int)(Math.random() * 4) + 1;
		Pet foePet2 = PetGenerator.getInstance().getPet(rand);
		foePet2.setLevel(minlevel + (maxLevel - minlevel));
		
		rand = (int)(Math.random() * 4) + 1;
		Pet foePet3 = PetGenerator.getInstance().getPet(rand);
		foePet3.setLevel(maxLevel);
		
		Player player1 = new Player();
		player1.setPet(foePet1);
		
		Player player2 = new Player();
		player2.setPet(foePet2);
		
		Player player3 = new Player();
		player3.setPet(foePet3);
		
		Core.getInstance().foe.add(player1);
		Core.getInstance().foe.add(player2);
		Core.getInstance().foe.add(player3);
		
		Core.getInstance().winningGold = maxLevel * 100;
		
		BattleInfo.getInstance().rankID = rankID;
		
		switchScreen(new BattleScreen());
	}

	private void createList(DisplayObject listWindow ,ArrayList<DisplayText> list) {
		for(int i=0; i < list.size();i++) {
			DisplayText listItem = list.get(i);
			float x = 20;
			float y = listWindow.getHeight() - 80 - (i * 40);
			listItem.setPreferedWidth((listWindow.getWidth()) - 40);
			listItem.setAlignment(HAlignment.LEFT);
			listItem.setPosition(x ,y);
			listWindow.addActor(listItem);
		}
	}

	private DisplayObject createWindow(String textureName , float x, float y ,  float width , float height ) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		final NinePatch texture = atlas.createPatch(textureName);
		
		DisplayObject window = new DisplayObject(){
			@Override
			public void draw(Batch batch, float parentAlpha) {
				texture.draw(batch, getX(), getY(), getWidth(), getHeight());
				super.draw(batch, parentAlpha);
			}
		};
		window.setPosition(x, y);
		window.setSize(width, height);
//		window.addListener(new ActorDragListener());
		return window;
	}
	
	private DisplayText createText(String string, float x, float y,
			BitmapFont font) {

		DisplayText textDisplay = new DisplayText(string, font);
		textDisplay.setColor(Color.BLACK);
		textDisplay.setPosition(x, y);
		//textDisplay.addListenerebug = true;
//		textDisplay.addListener(new ActorDragListener());
		return textDisplay;
	}
	
	private void highLight(ArrayList<DisplayText> list ,DisplayText text) {
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getTouchable() == Touchable.disabled) {
				list.get(i).setColor(Color.GRAY);
			} else {
				list.get(i).setColor(Color.BLACK);
			}
		}
		for(int i=0;i<list.size();i++) {
			if(text == list.get(i) && list.get(i).getTouchable()== Touchable.enabled) {
				list.get(i).setColor(Color.BLUE);
			}
		}
	}
	
	private void disableText(DisplayText text){
		text.setColor(Color.GRAY);
		text.setTouchable(Touchable.disabled);
	}

	@Override
	public void received(Object obj,Connection con) {
		if(NetworkData.getInstance().isServer) {
			if (obj instanceof GameRequest) {
		        GameRequest request = (GameRequest)obj;
		        if(request.type == RequestType.GET_PLAYER_INFO) {
		        	setFoe(request);
		        	
		        	
		        	NetworkData.getInstance().state = NetworkData.SHOW_PLAYERS_DATA;
		        	GameRequest response = new GameRequest();
		        	response.type = RequestType.GET_PLAYER_INFO;
		        	response.playerData = Core.getInstance().player.getPlayerData();
		        	response.petData = Core.getInstance().player.getPet().getPetData();
		        	con.sendTCP(response);
		        }
		    }
		} else if(NetworkData.getInstance().isClient) {
			if (obj instanceof GameRequest) {
				GameRequest request = (GameRequest)obj;
		        if(request.type == RequestType.GET_PLAYER_INFO) {
		        	setFoe(request);
		        	
		        	NetworkData.getInstance().state = NetworkData.SHOW_PLAYERS_DATA;
//		        	GameRequest serverRequest = new GameRequest();
//		        	serverRequest.playerData = Core.getInstance().player.getPlayerData();
//		        	serverRequest.petData = Core.getInstance().player.getPet().getPetData();
		        }
		    }
		}
	}

	@Override
	public void disconnected(Connection con) {
	}

	
	
	
	public void connected(Connection con) {
		if(NetworkData.getInstance().isServer) {
			
		} else if(NetworkData.getInstance().isClient) {
			GameRequest request = new GameRequest();
			request.type = RequestType.GET_PLAYER_INFO;
			request.playerData = Core.getInstance().player.getPlayerData();
			request.petData = Core.getInstance().player.getPet().getPetData();
			con.sendTCP(request);
		} 
	}
	
	private void setFoe(GameRequest request) {
		Player player = new Player();
		player.setName(request.playerData.name);
		player.setRank(request.playerData.rank);
		
		PetData petData = request.petData;
		Pet pet = PetGenerator.getInstance().getPet(petData.id);
		pet.applyPetData(petData);
		
		player.setPet(pet);
		Core.getInstance().foe.clear();
		Core.getInstance().foe.add(player);
	}

	@Override
	public void connected(Connection con, Server server) {
		connected(con);
	}

	@Override
	public void connected(Connection con, Client client) {
		connected(con);
	}

}
