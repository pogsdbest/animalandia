package com.game.animalia.display.object.pet;

public class PetGenerator {

	public static PetGenerator instance;

	public static PetGenerator getInstance() {
		if (instance == null) {
			instance = new PetGenerator();
		}
		return instance;
	}

	public Pet getPet(int id) {
		Pet pet = null;
		switch (id) {
		case 1:
			pet = new Dog();
			pet.id = 1;
			break;
		case 2:
			pet = new Cat();
			pet.id = 2;
			break;
		case 3:
			pet = new Bird();
			pet.id = 3;
			break;
		case 4:
			pet = new Cow();
			pet.id = 4;
			break;

		default:
			break;
		}
		pet.setBaseSkills();
		pet.setBaseStats();
		pet.currentHealth = pet.health;
		
		return pet;
	}
}
