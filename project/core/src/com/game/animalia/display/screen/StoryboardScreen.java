package com.game.animalia.display.screen;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.game.animalia.Assets;
import com.game.animalia.util.Core;
import com.game.framework.display.DisplayObject;

public class StoryboardScreen extends AbstractScreen {
	
	private int slides;
	private int counter = 1;
	
	public StoryboardScreen(String bgName,boolean petStory) {
		super();
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		
		if(petStory) {
			TextureRegion bg = atlas.findRegion(bgName);
			DisplayObject bgDisplay = new DisplayObject(bg);
			addActor(bgDisplay);
			slides = 1;
			bgDisplay.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					switchScreen(new ArenaScreen());
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		} else {
			TextureRegion bg = atlas.findRegion("storyboardbg");
			DisplayObject bgDisplay = new DisplayObject(bg);
			addActor(bgDisplay);
			slides = 7;
			showSlides();
		}
		
		initScreen();
	}
	
	private void showSlides() {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion slideTexture = atlas.findRegion("story"+counter);
		final DisplayObject slide = new DisplayObject(slideTexture);
		slide.setPosition( ( stage.getWidth() - slide.getWidth() )/2, ( stage.getHeight() - slide.getHeight() )/2);
		addActor(slide);
		
		slide.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				slide.removeListener(this);
				slide.remove();
				counter++;
				if(counter >= slides) {
					switchScreen(new BattleScreen());
				} else {
					showSlides();
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}

}
