package com.game.animalia.network;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;

public interface NetworkInterface {

	public void received(Object obj,Connection con);
	
	public void disconnected(Connection con);
	
	public void connected(Connection con,Server server);
	public void connected(Connection con,Client client);
	
}
