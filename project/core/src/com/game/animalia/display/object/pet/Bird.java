package com.game.animalia.display.object.pet;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.game.animalia.Assets;
import com.game.animalia.display.object.skills.Fly;
import com.game.animalia.display.object.skills.Peck;

public class Bird extends Pet{
	
	public Bird() {
		super("Bird");
		
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		display = atlas.findRegion("bird");
		
		description = "an animal who can fly. that shits above.";
	}
	
	@Override
	public void setBaseStats(int level) {
		this.level = level;
		health = 40 + level * 3;
		attack = 15 + level * 2;
		defense = 5 + (int)(level * .5);
		speed = 8 + level * 2;
		accuracy = 10 + level * 2;
	}

	@Override
	public void setBaseSkills() {
		Peck peck = new Peck();
		skills.add(peck);
		skills.add(new Fly());
	}

}
