package com.game.animalia.network;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.game.animalia.display.object.skills.PetSkill;
import com.game.animalia.util.Player;
import com.game.framework.utils.L;

public class GameNetwork {
	
	public static final int TCP_PORT = 54555;
	public static final int UDP_PORT = 54777;
	
	
	public GameNetwork() {
		
	}
	
	public static void startServer(final NetworkInterface net) throws IOException {
		final Server server = new Server(){
			@Override
			protected Connection newConnection() {
				L.wtf("server connected!");
				return super.newConnection();
			}
			
		};
		Kryo kryo = server.getKryo();
	    kryo.register(GameRequest.class);
	    kryo.register(PlayerData.class);
	    kryo.register(PetData.class);
	    kryo.register(int[].class);
	    kryo.register(TurnData.class);
	    
	    server.start();
	    server.bind(TCP_PORT, UDP_PORT);
	    server.addListener(new Listener(){
	    	@Override
	    	public void received(Connection con, Object obj) {
	    		net.received(obj,con);
	    		super.received(con, obj);
	    	}
	    	
	    	@Override
	    	public void disconnected(Connection con) {
	    		net.disconnected(con);
	    		super.disconnected(con);
	    	}
	    	
	    	@Override
	    	public void connected(Connection con) {
	    		net.connected(con,server);
	    		super.connected(con);
	    	}
	    });
	}
	
	public static void connectTo(String ip,final NetworkInterface net) throws IOException {
		final Client client = new Client();
		Kryo kryo = client.getKryo();
		kryo.register(GameRequest.class);
		kryo.register(PlayerData.class);
		kryo.register(PetData.class);
		kryo.register(int[].class);
		kryo.register(TurnData.class);
		
	    client.start();
	    client.addListener(new Listener(){
	    	@Override
	    	public void connected(Connection con) {
	    		L.wtf("client connected!");
	    		net.connected(con,client);
	    		super.connected(con);
	    	}
	    	
	    	@Override
	    	public void received(Connection con, Object obj) {
	    		net.received(obj,con);
	    		super.received(con, obj);
	    	}
	    	
	    	@Override
	    	public void disconnected(Connection con) {
	    		net.disconnected(con);
	    		super.disconnected(con);
	    	}
	    });
	    
	    client.connect(5000, ip, TCP_PORT, UDP_PORT);
	    
	}
	
	public static List<InetAddress> discoverHost() {
		Client client = new Client();
		List<InetAddress> hosts = client.discoverHosts(UDP_PORT, 5000);
		return hosts;
	}

}
