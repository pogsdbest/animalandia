package com.game.animalia.network;

public class PetData {
	
	public int id;
	public String name;
	
	public int level;
	public int health;
	public int attack;
	public int defense;
	public int speed;
	public int accuracy;
	
	public int[] skills;

}
