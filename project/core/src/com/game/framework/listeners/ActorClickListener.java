package com.game.framework.listeners;

import com.game.framework.display.DisplayObject;

public interface ActorClickListener {
	
	public void clicked(DisplayObject object);

}
