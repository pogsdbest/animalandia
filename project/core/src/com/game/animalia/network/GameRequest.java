package com.game.animalia.network;


public class GameRequest {
	
	public int type;
	public float randomSeed;
	public PlayerData playerData;
	public PetData petData;
	public TurnData playerTurnData;
	public TurnData foeTurnData;
}
