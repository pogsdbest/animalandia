package com.game.animalia.display.screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.game.animalia.Assets;
import com.game.animalia.display.object.TapCallback;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.animalia.display.object.pet.PetGenerator;
import com.game.animalia.util.Core;
import com.game.animalia.util.KeyboardInputPanel;
import com.game.animalia.util.Player;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.listeners.ActorClickListener;

public class NewGameScreen extends AbstractScreen {
	
	private BitmapFont font;
	private DisplayObject dialogueWindow;
	private KeyboardInputPanel keyboard;
	private Player player;
	private PetDisplay selectedPet;
	private Group petGroup;
	private DisplayText restartText;

	public NewGameScreen() {
		super();
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");

		font = Assets.getInstance().generateFont(20, Assets.getInstance().charSet);
		
		TextureRegion bg = atlas.findRegion("battlescreenbg");
		DisplayObject bgDisplay = new DisplayObject(bg);
		addActor(bgDisplay);
		
		restartText = createText("Back", 681,25);
		restartText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				clear();
				start();
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		//restartText.addListener(new ActorDragListener());
		
		createDialogueWindow();
		//addActor(restartText);
		addKeyboard();
		
		start();
		initScreen();
		
		Assets.getInstance().playMusic("sfx/mainmenubgm.mp3");
	}
	
	private void start() {
		
		Core.getInstance().isTutorial = true;
		player = new Player();
		showDialogue("hello trainer! what is your name?\n(Tap to continue)", new TapCallback() {
			@Override
			public void tap() {
				showDialogue("please input your name.", new TapCallback() {
					
					@Override
					public void tap() {
					}
				});
				keyboard.showKeyboard();
				keyboard.reset();
				keyboard.doneBtnDisplay.setClickedListener(new ActorClickListener(){
					@Override
					public void clicked(DisplayObject obj) {
						player.setName(keyboard.textDisplay.getText());
						selectPet();
						keyboard.hideKeyboard();
						keyboard.doneBtnDisplay.setClickedListener(null);
					}
				});
			}
		});
		restartText.setVisible(false);
	}
	
	private void clear() {
		keyboard.doneBtnDisplay.setClickedListener(null);
		keyboard.hideKeyboard();
		if(petGroup!=null) {
			petGroup.remove();
			petGroup = null;
		}
		if(selectedPet!=null) {
			selectedPet.remove();
			selectedPet = null;
		}
		
	}
	
	private void selectPet() {
		showDialogue("ok "+player.getName()+" please select your pet.", new TapCallback() {
			@Override
			public void tap() {
				
			}
		});
		petGroup = new Group();
		addActor(petGroup);
		
		PetDisplay dogDisplay = createPetDisplay(PetGenerator.getInstance().getPet(1),448,231);
		petGroup.addActor(dogDisplay);
		
		PetDisplay catDisplay = createPetDisplay(PetGenerator.getInstance().getPet(2),670,232);
		petGroup.addActor(catDisplay);
		
		PetDisplay birdDisplay = createPetDisplay(PetGenerator.getInstance().getPet(3),285,232);
		petGroup.addActor(birdDisplay);
		
		PetDisplay cowDisplay = createPetDisplay(PetGenerator.getInstance().getPet(4),35,228);
		petGroup.addActor(cowDisplay);
		
	}
	
	private PetDisplay createPetDisplay(final Pet pet,float x,float y) {
		final PetDisplay display = new PetDisplay(pet);
		display.setPosition(x,y);
//		display.addListener(new ActorDragListener());
		display.addListener( new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(selectedPet==display) {
					setPlayerPet(pet);
					petGroup.remove();
					
				} else {
					selectedPet = display;
					showDialogue(pet.description, new TapCallback() {
						
						@Override
						public void tap() {
						}
					});
				}
				
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		return display;
	}
	
	protected void setPlayerPet(final Pet pet) {
		player.setPet(pet);
		addActor(selectedPet);
		selectedPet.setPosition(341,178);
		
//		selectedPet.addListener(new ActorDragListener());
		showDialogue("you select a type "+pet.name, new TapCallback() {
			
			@Override
			public void tap() {
				showDialogue("what will be your pet's name?", new TapCallback() {
					
					@Override
					public void tap() {
					}
				});
				keyboard.showKeyboard();
				keyboard.reset();
				keyboard.doneBtnDisplay.setClickedListener(new ActorClickListener(){
					@Override
					public void clicked(DisplayObject obj) {
						keyboard.hideKeyboard();
						keyboard.doneBtnDisplay.setClickedListener(null);
						pet.name = keyboard.textDisplay.getText();
						showDialogue("wow '"+pet.name+"' is a good name.", new TapCallback() {
							
							@Override
							public void tap() {
								showDialogue("ok "+player.getName()+" now that everything's ready. let me teach you how to fight with your pet "+pet.name+".", new TapCallback() {
									
									@Override
									public void tap() {
										Core.getInstance().player = player;
										Core.getInstance().foe.add(createFoeForTestPurpose());
										Assets.getInstance().stopMusic();
										//switchScreen(new BattleScreen());
										switchScreen(new StoryboardScreen("",false));
									}
								});
							}
						});
					}
				});
			}
		});
	}

	private void showDialogue(String text,final TapCallback callback) {
		dialogueWindow.clearChildren();
		dialogueWindow.setVisible(true);
		restartText.setVisible(true);
		
		DisplayText dialogueText = createText(text, 20, 20);
		dialogueText.setPreferedWidth(dialogueWindow.getWidth() - 40);
		dialogueText.setAlignment(HAlignment.LEFT);
		dialogueText.setPosition(20, dialogueWindow.getHeight() - 20 - dialogueText.getTextHeight());
		dialogueWindow.addActor(dialogueText);
		
		dialogueWindow.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				dialogueWindow.removeListener(this);
				callback.tap();
				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}
	
	private DisplayText createText(String string, float x, float y) {

		DisplayText textDisplay = new DisplayText(string, font);
		textDisplay.setColor(Color.BLACK);
		textDisplay.setPosition(x, y);
		// textDisplay.isDebug = true;
//		 textDisplay.addListener(new ActorDragListener());
		return textDisplay;
	}

	private void addKeyboard() {
		keyboard = new KeyboardInputPanel(stage,manager);
		addActor(keyboard);
	}

	private void createDialogueWindow() {
		dialogueWindow = createWindow("window1",25,10);
		
		dialogueWindow.setVisible(false);
	}
	
	private DisplayObject createWindow(String textureName,int x,int y) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion texture = atlas.findRegion(textureName);
		DisplayObject window = new DisplayObject(texture);
		addActor(window);
		window.setPosition(x,y);
		//window.addListener(new ActorDragListener());
		return window;
	}
	
	private Player createFoeForTestPurpose() {
		Player foe = new Player();
		int rand = (int)(Math.random() * 4) + 1;
		Pet pet = PetGenerator.getInstance().getPet(rand);
		pet.health = 20;
//		pet.attack = 999;
		pet.currentHealth = pet.health;
		pet.speed = 0;
		pet.accuracy = 99;
		pet.getSkills().remove(1);
		foe.setPet(pet);
		return foe;
	}
}
