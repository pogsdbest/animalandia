package com.game.animalia.display.object;

import com.game.animalia.display.object.items.Item;
import com.game.animalia.display.object.skills.PetSkill;

public class Turn {
	
	public enum TurnType {
		SKILL,ITEM,RUN,SWITCH,DEFEND
	}
	
	private TurnType turnType;
	
	private PetSkill petSkill;
	private Item item;
	public boolean isDone;
	
	public void setTurn(TurnType type,PetSkill petSkill){
		this.turnType = type;
		this.petSkill = petSkill;
	}
	
	public void setTurn(TurnType type,Item item) {
		this.turnType = type;
		this.item = item;
	}
	
	public TurnType getTurnType() {
		return this.turnType;
	}
	
	public PetSkill getPetSkill() {
		return petSkill;
	}
	
	public Item getItem() {
		return this.item;
	}

}
