package com.game.framework.display.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ImageTextButton extends ImageButton {

	private BitmapFont font;
	private String text;
	
	public ImageTextButton(TextureRegion buttonUp, TextureRegion buttonDown,String text) {
		super(buttonUp, buttonDown);
		this.setText(text);
		
		font = new BitmapFont();
	}
	
	public ImageTextButton(TextureRegion buttonUp, TextureRegion buttonDown,TextureRegion buttonDisabled,String text) {
		super(buttonUp, buttonDown,buttonDisabled);
		this.setText(text);
		
		font = new BitmapFont();
	}
	
	public void setFont(BitmapFont font) {
		this.font = font;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		super.draw(batch, parentAlpha);
		float fontHeight = font.getMultiLineBounds(getText()).height;
		float centerY = getY() + getHeight()/2 + fontHeight/2;
		
		font.drawWrapped(batch, getText(), getX(), centerY, getWidth() * getScaleX() , HAlignment.CENTER);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	

}
