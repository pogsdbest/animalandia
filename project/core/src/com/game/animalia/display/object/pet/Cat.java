package com.game.animalia.display.object.pet;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.game.animalia.Assets;
import com.game.animalia.display.object.skills.NineLives;
import com.game.animalia.display.object.skills.Scratch;

public class Cat extends Pet {

	public Cat() {
		super("Cat");

		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		display = atlas.findRegion("cat");
		
		description = "stupid animal who doesn't listen to you.";
	}
	
	@Override
	public void setBaseStats(int level) {
		this.level = level;
		health = 50 + level * 4;
		attack = 15 + level * 2;
		defense = 5 + level * 1;
		speed = 5 + (int)(level * .5);
		accuracy = 5 + level * 2;
	}

	@Override
	public void setBaseSkills() {
		Scratch scratch = new Scratch();
		skills.add(scratch);
		skills.add(new NineLives());
	}

}
