package com.game.animalia.display.screen;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.game.animalia.Assets;
import com.game.animalia.display.object.ActionFinishedCallback;
import com.game.animalia.display.object.TapCallback;
import com.game.animalia.display.object.items.Item;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.animalia.display.object.skills.PetSkill;
import com.game.animalia.util.BattleInfo;
import com.game.animalia.util.Core;
import com.game.animalia.util.Player;
import com.game.animalia.util.Setting;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.display.ui.ImageButton;
import com.game.framework.listeners.ActorClickListener;
import com.game.framework.listeners.ActorDragListener;
import com.game.framework.utils.L;

public class HomeScreen extends AbstractScreen {
	
	private PetDisplay petDisplay;
	private DisplayObject dialogueWindow;
	private DisplayObject infoWindow;
	private DisplayText selected;
	private BitmapFont font;
	private BitmapFont font2;
	
	private ArrayList<DialogInfo> tutorialDialogues;
	private DisplayText backText; 
	
	public class DialogInfo {
		String text;
		TapCallback callback;
		
	}
	
	public HomeScreen() {
		super();
		
		font = Assets.getInstance().generateFont(20, Assets.getInstance().charSet);
		font2 = Assets.getInstance().generateFont(13, Assets.getInstance().charSet);
		Assets.getInstance().playMusic("sfx/home.mp3");
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		
		TextureRegion bg = atlas.findRegion("homebg"+Core.getInstance().player.getPet().id);
		DisplayObject mapBgDisplay = new DisplayObject(bg);
		mapBgDisplay.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				infoWindow.setVisible(false);
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		addActor(mapBgDisplay);
		
		addPetDisplay();
		addLifeAndExpWindow();
		
		addButtons();
		
		addInfoWindow();
		addDialogueWindow();
		
		if(Core.getInstance().isTutorial) {
			tutorialDialogues = new ArrayList<HomeScreen.DialogInfo>();
			showTutorialDialogue();
		}
		initScreen();
		
		
	}

	private void showTutorialDialogue() {
		final Player player = Core.getInstance().player;
		showPetStats();
		showDialogue("good day "+player.getName()+"! here is your home. you can check the stats of your pet by clicking the stats button.", new TapCallback() {
			
			@Override
			public void tap() {
				showPetSkills();
				showDialogue("you can also view your pet's skills on the skill window.", new TapCallback() {
					
					@Override
					public void tap() {
						showPlayerItems();
						showDialogue("last time i gave you an item which you can use in the middle of a match. don't worry you can get that again by visiting the shop.", new TapCallback() {
							
							@Override
							public void tap() {
								showMenu();
								showDialogue("last on the list is the Menu where you can save your progress by tapping Save twice.", new TapCallback() {
									
									@Override
									public void tap() {
										infoWindow.setVisible(false);
										showDialogue("that's all "+player.getName()+". now you can explore on your own and train your pet and challenge other trainers on the arena.", new TapCallback() {
											
											@Override
											public void tap() {
												player.setGold(500);
												Core.getInstance().isTutorial = false;
												showDialogue("here take this 500 gold as a gift. see you!", new TapCallback() {
													
													@Override
													public void tap() {
														backText.setVisible(false);
														removeListener();
														tutorialDialogues.clear();
														dialogueWindow.setVisible(false);
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});
	}

	private void addButtons() {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion stats = atlas.findRegion("statsb");
		TextureRegion skills = atlas.findRegion("skillsb");
		TextureRegion items = atlas.findRegion("itemsb");
		TextureRegion town = atlas.findRegion("townb");
		TextureRegion menu = atlas.findRegion("menub");
		TextureRegion tips = atlas.findRegion("tipsb");
		
		Group group = new Group();
		ImageButton statsB = createButton(stats, 20, 10);
		statsB.setClickedListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				Core.getInstance().save();
				showPetStats();
			}
		});
		ImageButton skillsB = createButton(skills, 150, 10);
		skillsB.setClickedListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				showPetSkills();
			}
		});
		ImageButton itemsB = createButton(items, 290, 10);
		itemsB.setClickedListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				showPlayerItems();
			}
		});
		ImageButton townB = createButton(town, 670,10);
		townB.setClickedListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				Assets.getInstance().stopMusic();
				switchScreen(new MapScreen());
			}
		});
		ImageButton menuB = createButton(menu,663,407);
		menuB.setClickedListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				showMenu();
			}
		});
		
		ImageButton tipsB = createButton(tips,427,10);
		tipsB.setClickedListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				showTips();
			}
		});
		
		backText = createText("Back", 28,165, 20);
		backText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				backDialogue();
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		backText.setVisible(false);
		//backText.addListener(new ActorDragListener());
		
		group.addActor(statsB);
		group.addActor(skillsB);
		group.addActor(itemsB);
		group.addActor(townB);
		group.addActor(menuB);
		group.addActor(tipsB);
		group.addActor(backText);
		
		addActor(group);
	}
	
	protected void showTips() {
		infoWindow.setVisible(true);
		infoWindow.clearChildren();
		
		String text = "Game Tips";
		DisplayText tipsText = createText(text, 15, 0, 20);
		tipsText.setY(infoWindow.getHeight() - 20 - tipsText.getTextHeight());
		infoWindow.addActor(tipsText);
		
		text = " you can use the earned XP and golds in the shop to buy potions and pet food. "+
   "you can win the battle by training your pets at the Academy."+
   "you can heal your pet right after the battle by bringing them into the hospital."+
   "the higher the level of your pet,the more chance you can win the battle."+
   "you can use potions and pet foods during the battle to increase the chance of winning."+
   "after every battle,your pet gains XP and golds that can bring your pet to a higher level."+
   "when your pet is defeated,the hospital autonatically cure your pet to fight again."+
	"enjoy! ";
		DisplayText tipsContent = createText(text, 15, 0, font2);
		tipsContent.setPreferedWidth(infoWindow.getWidth() - 20);
		tipsContent.setY(infoWindow.getHeight() - 50 - tipsContent.getTextHeight());
		infoWindow.addActor(tipsContent);
	}

	protected void showMenu() {
		infoWindow.setVisible(true);
		infoWindow.clearChildren();
		
		String text = "Menu";
		DisplayText menuText = createText(text, 15, 0, 20);
		menuText.setY(infoWindow.getHeight() - 20 - menuText.getTextHeight());
		infoWindow.addActor(menuText);
		
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		Setting setting = Core.getInstance().setting;
		String onOff = (setting.sound) ? "ON" : "OFF";
		final DisplayText soundText = createText(">Sound "+onOff, 0, 0, font);
		soundText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Setting setting = Core.getInstance().setting;
				if(selected == soundText) {
					setting.sound = !setting.sound;
					if(setting.sound) {
						Assets.getInstance().playMusic();
					} else {
						Assets.getInstance().stopMusic();
					}
					String onOff = (setting.sound) ? "ON" : "OFF";
					soundText.setText(">Sound "+onOff);
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		final DisplayText saveText = createText(">Save", 0, 0, font);
		saveText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(selected == saveText) {
					Core.getInstance().save();
					saveText.setColor(Color.GRAY);
					saveText.setTouchable(Touchable.disabled);
					saveText.setText("-Saved-");
					selected = null;
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		final DisplayText quitText = createText(">Quit", 0, 0, font);
		quitText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(selected == quitText) {
					Assets.getInstance().stopMusic();
					switchScreen(new MainMenuScreen());
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		list.add(soundText);
		list.add(saveText);
		list.add(quitText);
		
		for(int i =0;i<list.size();i++) {
			final DisplayText textDisplay = list.get(i);
			textDisplay.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					if(textDisplay.getTouchable() == Touchable.disabled) return false;
					selected = textDisplay;
					highLight(list, textDisplay);
					return super.touchDown(event, x, y, pointer, button);
				}
			});
			
		}
		createList(infoWindow, list);
	}

	protected void showPlayerItems() {
		infoWindow.setVisible(true);
		infoWindow.clearChildren();
		
		String text = "Items";
		DisplayText itemsText = createText(text, 15, 0, 20);
		itemsText.setY(infoWindow.getHeight() - 20 - itemsText.getTextHeight());
		infoWindow.addActor(itemsText);
		
		String desc = "Descrption: ";
		DisplayText descText = createText(desc, 15, 0, 20);
		descText.setPosition(15, 150);
		infoWindow.addActor(descText);
		
		String charSet = Assets.getInstance().charSet;
		final DisplayText descValue = createText(charSet, 15, 0, 20);
		descValue.setText("available items on the backpack. double tap to use.");
		descValue.setPreferedWidth(infoWindow.getWidth() - 30);
		descValue.setPosition(15, 130 - descValue.getTextHeight());
		infoWindow.addActor(descValue);
		
		ArrayList<Item> list = Core.getInstance().player.getItems();
		final ArrayList<DisplayText> textList = new ArrayList<DisplayText>();
		
		for(int i =0;i<list.size();i++) {
			final DisplayText displayText = createText(">"+list.get(i).name+" x"+list.get(i).quantity, 0, 0, 20);
			textList.add(displayText);
			final Item item = list.get(i);
			displayText.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					highLight(textList, displayText);
					descValue.setText(item.desc);
					descValue.setY(130 - descValue.getTextHeight());
					if(selected==displayText) {
						useItem(item);
					} else {
						selected = displayText;
					}
					return super.touchDown(event, x, y, pointer, button);
				}
			});
			
		}
		createList(infoWindow, textList);
	}

	protected void useItem(Item item) {
		Pet pet = Core.getInstance().player.getPet();
		showDialogue(item.getText(pet), new TapCallback() {
			
			@Override
			public void tap() {
				dialogueWindow.clearChildren();
				dialogueWindow.setVisible(false);
			}
		});
		item.used();
		petDisplay.addAction(item.getAction(pet, new ActionFinishedCallback() {
			
			@Override
			public void finished() {
			}
		}));
		if(item.quantity == 0) {
			Core.getInstance().player.getItems().remove(item);
		}
		showPlayerItems();
	}

	protected void showPetSkills() {
		infoWindow.setVisible(true);
		infoWindow.clearChildren();
		
		Pet pet = Core.getInstance().player.getPet();
		String text = "Skills";
		DisplayText skillsText = createText(text, 15, 0, 20);
		skillsText.setY(infoWindow.getHeight() - 20 - skillsText.getTextHeight());
		infoWindow.addActor(skillsText);
		
		String desc = "Descrption: ";
		DisplayText descText = createText(desc, 15, 0, 20);
		descText.setPosition(15, 150);
		infoWindow.addActor(descText);
		
		String charSet = Assets.getInstance().charSet;
		final DisplayText descValue = createText(charSet, 15, 0, 20);
		descValue.setText(pet.name+"'s available skills.");
		descValue.setPreferedWidth(infoWindow.getWidth() - 30);
		descValue.setPosition(15, 130 - descValue.getTextHeight());
		infoWindow.addActor(descValue);
		
		ArrayList<PetSkill> list = pet.getSkills();
		final ArrayList<DisplayText> textList = new ArrayList<DisplayText>();
		
		for(int i =0;i<list.size();i++) {
			final DisplayText displayText = createText(">"+list.get(i).name+" lv."+list.get(i).level, 0, 0, 20);
			textList.add(displayText);
			final PetSkill skill = list.get(i);
			displayText.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					highLight(textList, displayText);
					descValue.setText(skill.desc);
					descValue.setY(130 - descValue.getTextHeight());
					return super.touchDown(event, x, y, pointer, button);
				}
			});
			
		}
		createList(infoWindow, textList);
		
	}
	
	private void createList(DisplayObject listWindow ,ArrayList<DisplayText> list) {
		for(int i=0; i < list.size();i++) {
			DisplayText listItem = list.get(i);
			float x = 15;
			float y = listWindow.getHeight() - 80 - (i * 40);
			listItem.setPreferedWidth(listWindow.getWidth());
			listItem.setAlignment(HAlignment.LEFT);
			listItem.setPosition(x ,y);
			listWindow.addActor(listItem);
		}
		
	}
	
	private void highLight(ArrayList<DisplayText> list ,DisplayText text) {
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getTouchable() == Touchable.disabled) {
				list.get(i).setColor(Color.GRAY);
			} else {
				list.get(i).setColor(Color.BLACK);
			}
		}
		for(int i=0;i<list.size();i++) {
			if(text == list.get(i)) {
				list.get(i).setColor(Color.BLUE);
			}
		}
	}

	protected void showPetStats() {
		infoWindow.setVisible(true);
		infoWindow.clearChildren();
		Pet pet = Core.getInstance().player.getPet();
		String text = 
				"Stats\n\n" +
				"Name: "+pet.name+"\n"+
				"Level: "+pet.level+"\n\n"+
				"Exp: "+pet.currentExp+"/"+pet.getNextExp()+"\n"+
				"Health: "+pet.currentHealth+"/"+pet.health+"\n"+
				"Attack: "+pet.attack+"\n"+
				"Defense: "+pet.defense+"\n"+
				"Speed: "+pet.speed+"\n"+
				"Accuracy: "+pet.accuracy+"\n\n"+
				"Description:\n"+pet.description;
		DisplayText statsText = createText(text, 0, 0, 20);
		statsText.setPreferedWidth(infoWindow.getWidth() - 30);
		statsText.setAlignment(HAlignment.LEFT);
		statsText.setPosition(15, infoWindow.getHeight() - 20 - statsText.getTextHeight());
		infoWindow.addActor(statsText);
	}

	private void addInfoWindow() {
		infoWindow = createWindow("window6",430,70,350,400);
		addActor(infoWindow);
		infoWindow.setVisible(false);
	}

	private void addDialogueWindow() {
		dialogueWindow = createWindow("window6",20,5,760,150);
		addActor(dialogueWindow);
		dialogueWindow.setVisible(false);
	}

	private void addLifeAndExpWindow() {
		final Player player = Core.getInstance().player;
		Pet pet = petDisplay.getPet();
		DisplayObject window = createWindow("window6",10,370,300,100);
		DisplayText playerName = createText(player.getName(), 13,70, 20);
		DisplayText playerRank = createText("Rank: "+BattleInfo.getInstance().getRank(player.getRank()), 13, 40, 20);
		final DisplayText gold = createText("Gold: "+player.getGold(), 13, 10, 20);
		gold.setPreferedWidth(window.getWidth() - 26);
		gold.addAction(new Action() {
			
			@Override
			public boolean act(float delta) {
				gold.setText("Gold: "+player.getGold());
				return false;
			}
		});
		
		window.addActor(gold);
		window.addActor(playerRank);
		window.addActor(playerName);
		addActor(window);
	}

	private void addPetDisplay() {
		Pet pet = Core.getInstance().player.getPet();
		
		petDisplay = new PetDisplay(pet);
		petDisplay.setPosition(329,163);
		addActor(petDisplay);
		petDisplay.addListener(new ActorDragListener());
		
	}
	
	private DisplayObject createWindow(String textureName , float x, float y ,  float width , float height ) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		final NinePatch texture = atlas.createPatch(textureName);
		
		DisplayObject window = new DisplayObject(){
			@Override
			public void draw(Batch batch, float parentAlpha) {
				texture.draw(batch, getX(), getY(), getWidth(), getHeight());
				super.draw(batch, parentAlpha);
			}
		};
		window.setPosition(x, y);
		window.setSize(width, height);
		//window.addListener(new ActorDragListener());
		return window;
	}
	
	private DisplayText createText(String string, float x,float y,int size) {
		return createText(string, x, y, font);
	}
	
	private DisplayText createText(String string, float x,float y,BitmapFont font) {
		
		DisplayText textDisplay = new DisplayText(string,font);
		textDisplay.setColor(Color.BLACK);
		textDisplay.setPosition(x, y);
		//textDisplay.isDebug = true;
		//textDisplay.addListener(new ActorDragListener());
		return textDisplay;
	}
	
	private ImageButton createButton(TextureRegion texture,float x,float y) {
		ImageButton button = new ImageButton(texture, texture);
		button.setPosition(x, y);
//		button.addListener(new ActorDragListener());
		return button;
	}
	private void showDialogue(final String text,final TapCallback callback) {
		dialogueWindow.clearChildren();
		dialogueWindow.setVisible(true);
		
		DisplayText dialogueText = createText(text, 20, 20, 20);
		dialogueText.setPreferedWidth(dialogueWindow.getWidth() - 40);
		dialogueText.setAlignment(HAlignment.LEFT);
		dialogueText.setPosition(20, dialogueWindow.getHeight() - 20 - dialogueText.getTextHeight());
		dialogueWindow.addActor(dialogueText);
		
		
		dialogueWindow.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				dialogueWindow.removeListener(this);
				DialogInfo info = new DialogInfo();
				info.text = text;
				info.callback = callback;
				tutorialDialogues.add(info);
				backText.setVisible(true);
				L.wtf("add str = "+text);
				callback.tap();
				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}
	
	
	
	private void backDialogue() {
		
		if(tutorialDialogues.size() == 0) { 
			backText.setVisible(false);
			return;
		}
		removeListener();
		
		DialogInfo info = tutorialDialogues.get(tutorialDialogues.size() - 1);
		tutorialDialogues.remove(info);
		if(tutorialDialogues.size() == 0) backText.setVisible(false);
		L.wtf("remove: size = "+tutorialDialogues.size());
		showDialogue(info.text, info.callback);
	}
	
	private void removeListener() {
		for(EventListener listener: dialogueWindow.getListeners()) {
			dialogueWindow.removeListener(listener);
		}
	}

}
