package com.game.animalia.util;

import java.util.ArrayList;

import com.game.animalia.display.object.items.Item;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.skills.PetSkill;

public class Save {

	private String playerName;
	private Pet pet;
	private int petCurrentHealth;
	private int gold;
	private int rank;
	private ArrayList<Item> items;
	private ArrayList<PetSkill> skills;
	
	private boolean sound;
	private boolean isTutorial;

	/**
	 * @return the playerName
	 */
	public String getPlayerName() {
		return playerName;
	}

	/**
	 * @param playerName the playerName to set
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	/**
	 * @return the items
	 */
	public ArrayList<Item> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}

	/**
	 * @return the gold
	 */
	public int getGold() {
		return gold;
	}

	/**
	 * @param gold the gold to set
	 */
	public void setGold(int gold) {
		this.gold = gold;
	}

	/**
	 * @return the petCurrentHealth
	 */
	public int getPetCurrentHealth() {
		return petCurrentHealth;
	}

	/**
	 * @param petCurrentHealth the petCurrentHealth to set
	 */
	public void setPetCurrentHealth(int petCurrentHealth) {
		this.petCurrentHealth = petCurrentHealth;
	}

	/**
	 * @return the sound
	 */
	public boolean isSound() {
		return sound;
	}

	/**
	 * @param sound the sound to set
	 */
	public void setSound(boolean sound) {
		this.sound = sound;
	}

	/**
	 * @return the isTutorial
	 */
	public boolean isTutorial() {
		return isTutorial;
	}

	/**
	 * @param isTutorial the isTutorial to set
	 */
	public void setTutorial(boolean isTutorial) {
		this.isTutorial = isTutorial;
	}

	/**
	 * @return the skills
	 */
	public ArrayList<PetSkill> getSkills() {
		return skills;
	}

	/**
	 * @param skills the skills to set
	 */
	public void setSkills(ArrayList<PetSkill> skills) {
		this.skills = skills;
	}

	/**
	 * @return the pet
	 */
	public Pet getPet() {
		return pet;
	}

	/**
	 * @param pet the pet to set
	 */
	public void setPet(Pet pet) {
		this.pet = pet;
	}

	/**
	 * @return the rank
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * @param rank the rank to set
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}
	
	
}
