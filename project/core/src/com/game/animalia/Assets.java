package com.game.animalia;

import java.util.HashSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.game.animalia.util.Core;
import com.game.animalia.util.Setting;
import com.game.framework.utils.L;

public class Assets {

	private static Assets instance;
	public AssetManager manager;
	private FreeTypeFontGenerator defaultFontGenerator;
	public String charSet;
	private Music currentMusic;

	public static Assets getInstance() {
		if (instance == null) {
			instance = new Assets();
		}
		return instance;
	}

	public void init() {
		manager = new AssetManager();
		
		manager.load("gfx/assets.pack",TextureAtlas.class);
		//manager.load("gfx/activity.pack",TextureAtlas.class);
		
		manager.load("sfx/mainmenubgm.mp3",Music.class);
		manager.load("sfx/map.mp3",Music.class);
		manager.load("sfx/home.mp3",Music.class);
		manager.load("sfx/battle.mp3",Music.class);
		//manager.load("sfx/victory.mp3", Sound.class);
		//smanager.load("sfx/fail.mp3", Sound.class);
		
		defaultFontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("data/font/arcade.ttf"));
		charSet = getDefaultAllCharSet();
	}
	
	public <T> T get(String name) {
		if (!manager.isLoaded(name)) {
			L.e(name + "is not loaded");
		}
		return manager.get(name);
	}
	
	public void dispose() {
		manager.clear();
	}
	
	public BitmapFont generateFont(int size,String string) {
		BitmapFont font = defaultFontGenerator.generateFont(size, getUniqueChars(string), false);
		return font;
	}
	
	public String getDefaultAllCharSet() {
		String charSet = "";
		for (int c=1; c<128; c++) { 
			charSet += ((char)c); 
		}
		return charSet;
	}
	
	public String getUniqueChars(String s) {
		String string = s.replace("\\n","");
		HashSet<String> set = new HashSet<String>();
		for(int i=0;i<string.length();i++) {
			set.add(string.charAt(i)+"");
		}
		String out = "";
		for(String value : set) {
			out += value;
		}
		return out;
	}
	
	public void playMusic(String name) {
		
		Music music = get(name);
		if(currentMusic!=null) {
			currentMusic.stop();
		}
		currentMusic = music;
		music.setLooping(true);
		if(!Core.getInstance().setting.sound) return;
		music.play();
	}
	
	public void playMusic() {
		if(currentMusic!=null) {
			currentMusic.play();
		}
	}
	
	public void stopMusic() {
		if(currentMusic!=null) {
			currentMusic.stop();
		}
	}

}
