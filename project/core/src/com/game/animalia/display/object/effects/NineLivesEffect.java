package com.game.animalia.display.object.effects;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.game.animalia.Assets;
import com.game.animalia.display.object.TapCallback;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.animalia.display.screen.BattleScreen;
import com.game.framework.display.DisplayObject;

public class NineLivesEffect extends Effect {
	public static final String NAME = "9Lives";
	private PetDisplay display;
	public DisplayObject iconDisplay;
	
	public NineLivesEffect(PetDisplay display) {
		super(NAME);
		this.display = display;
		
		addActor(getDisplayObject());
		
	}
	
	public DisplayObject getDisplayObject() {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion fear = atlas.findRegion("ninelives");
		iconDisplay = new DisplayObject(fear);
		return iconDisplay;
	}
	
	@Override
	public void activateEffect(final BattleScreen screen,final EffectCallback callback) {
		Pet pet = display.getPet();
		if(pet.currentHealth == 1) {
			remove();
			display.getEffects().remove(this);
			screen.showDialogue(NAME+" buff worn out!", new TapCallback() {
				
				@Override
				public void tap() {
					callback.callback();
				}
			});
		} else {
			callback.callback();
		}
	}

}
