package com.game.animalia.util;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.Bounce;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.game.animalia.Assets;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.display.ui.ImageButton;
import com.game.framework.listeners.ActorClickListener;
import com.game.framework.utils.ActorTweenAccessor;
import com.game.framework.utils.L;

public class KeyboardInputPanel extends DisplayObject{
	
	public static int BACK_PRESS = 8;
	
	public String text = "";

	public DisplayText textDisplay;

	public TextureRegion doneBtn;
	public DisplayText sourceTextDisplay;

	public ImageButton doneBtnDisplay;
	
	public KeyboardInputPanel(Stage stage, TweenManager manager) {
		super(null);
		
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_";
		BitmapFont font = Assets.getInstance().generateFont(40, characters);
		textDisplay = new DisplayText("",font);
		textDisplay.setPreferedWidth(stage.getWidth() - 100);
		textDisplay.setAlignment(HAlignment.CENTER);
		textDisplay.setPosition(50, stage.getHeight() - 160);
		addActor(textDisplay);
		
		setSize(stage.getWidth() - 100, 50);
		
		stage.addListener(new InputListener(){
			@Override
			public boolean keyTyped(InputEvent event, char character) {
				int charInt = character;
				
				if(charInt == BACK_PRESS) {
					if(text.length() > 0) {
						text = text.substring(0,text.length() - 1);
					}
				} else {
					text += character;
				}
				L.wtf("text is "+text);
				textDisplay.setText(text);
				return super.keyTyped(event, character);
			}
		});
		
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		doneBtn = atlas.findRegion("check");
		doneBtnDisplay = new ImageButton(doneBtn, doneBtn, doneBtn);
		doneBtnDisplay.setOrigin(doneBtnDisplay.getWidth()/2, doneBtnDisplay.getHeight()/2);
		doneBtnDisplay.setScale(0);
		doneBtnDisplay.setPosition(stage.getWidth() - 100,stage.getHeight() - 170);
		addActor(doneBtnDisplay);
		Tween.to(doneBtnDisplay, ActorTweenAccessor.SCALE_XY, 1f)
	    .target(1,1)
	    .ease(Bounce.OUT)
	    .start(manager);
		doneBtnDisplay.setClickedListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				hideKeyboard();
				if(sourceTextDisplay!=null) {
					sourceTextDisplay.setText(text);
				}
			}
		});
		
		setVisible(false);
		
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		if(isVisible()) {
			
			batch.end();
			debugRenderer.begin(ShapeType.Filled);
			debugRenderer.setColor(Color.BLACK);
			debugRenderer.rect(50, getStage().getHeight() - 170 , getWidth(),getHeight());
			debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
			debugRenderer.end();
			batch.begin();
		}
		super.draw(batch, parentAlpha);
		
	}
	
	public void reset() {
		text = "";
		textDisplay.setText(text);
	}
	
	public void showKeyboard() {
		Gdx.input.setOnscreenKeyboardVisible(true);
		setVisible(true);
	}
	
	public void hideKeyboard() {
		Gdx.input.setOnscreenKeyboardVisible(false);
		setVisible(false);
	}

}
