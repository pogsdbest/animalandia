package com.game.animalia.display.screen;

import com.badlogic.gdx.assets.AssetManager;
import com.game.animalia.Assets;
import com.game.framework.manager.ScreenManager;
import com.game.framework.utils.L;

public class LoadingScreen extends AbstractScreen {
	
	public LoadingScreen() {
		// TODO Auto-generated constructor stub
		super();
		
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		super.render(delta);
		AssetManager manager = Assets.getInstance().manager;
		if(manager.update()) {
			
			ScreenManager.getInstance().setScreen(new MainMenuScreen());
		}
		L.wtf("progress is "+manager.getProgress() * 100+"%");
	}

}
