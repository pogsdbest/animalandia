package com.game.animalia.display.object.pet;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.game.animalia.Assets;
import com.game.animalia.display.object.skills.Bark;
import com.game.animalia.display.object.skills.Bite;

public class Dog extends Pet {
	
	public Dog() {
		super("Dog");
		
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		display = atlas.findRegion("dog");
		
		description = "the most friendly pet who likes to bite people.";
	}

	@Override
	public void setBaseStats(int level) {
		this.level = level;
		health = 50 + level * 5;
		attack = 15 + level * 3;
		defense = 5 + level * 1;
		speed = 5 + level * 1;
		accuracy = 5 + level * 2;
	}

	@Override
	public void setBaseSkills() {
		Bite bite = new Bite();
		//Growl growl = new Growl();
		skills.add(bite);
		skills.add(new Bark());
		//skills.add(growl);
	}
	
}
