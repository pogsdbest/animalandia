package com.game.animalia.display.object.pet;

import java.util.ArrayList;

import com.game.animalia.display.object.effects.Effect;
import com.game.framework.display.DisplayObject;

public class PetDisplay extends DisplayObject {
	
	private Pet pet;
	private ArrayList<Effect> effects = new ArrayList<Effect>();

	public PetDisplay(Pet pet) {
		super(pet.getDisplay());
		this.pet = pet;
	}
	
	public Pet getPet() {
		return pet;
	}
	
	public void addEffect(Effect effect) {
		if(hasEffect(effect.name)) return;
		getEffects().add(effect);
	}
	
	public boolean hasEffect(String name) {
		for(int i=0;i<effects.size();i++) {
			if(effects.get(i).name.equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	public Effect getEffect(String name) {
		for(int i=0;i<effects.size();i++) {
			if(effects.get(i).name.equals(name)) {
				return effects.get(i);
			}
		}
		return null;
	}

	/**
	 * @return the effects
	 */
	public ArrayList<Effect> getEffects() {
		return effects;
	}

	/**
	 * @param effects the effects to set
	 */
	public void setEffects(ArrayList<Effect> effects) {
		this.effects = effects;
	}
}
