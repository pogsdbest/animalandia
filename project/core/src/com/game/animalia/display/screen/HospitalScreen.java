package com.game.animalia.display.screen;

import java.util.ArrayList;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.game.animalia.Assets;
import com.game.animalia.display.object.Gauge;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.animalia.util.Core;
import com.game.animalia.util.Player;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;

public class HospitalScreen extends AbstractScreen {
	
	private BitmapFont font;
	private DisplayText selected;
	private Group dialogueWindow;
	private PetDisplay petDisplay;
	private DisplayText healText;
	private DisplayText exitText;

	public HospitalScreen() {
		super();
		font = Assets.getInstance().generateFont(20, Assets.getInstance().charSet);
		Assets.getInstance().playMusic("sfx/home.mp3");
		//bgm.play();
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		
		TextureRegion bg = atlas.findRegion("battlescreenbg");
		DisplayObject mapBgDisplay = new DisplayObject(bg);
		addActor(mapBgDisplay);
		
		addMenuWindow();
		addDialogueWindow();
		addPetDisplay();
		addHealthBar();
		
		Pet pet = Core.getInstance().player.getPet();
		if(pet.isFaint) {
			String dialogue[] = {
					"look who's here! another weak trainer who lose in battle!",
					"omg what happen to your pet? we need to heal them quick.",
					"Don't worry dude. everyone loses in battle.let's heal your pet"};
			int rand = (int)(Math.random() * dialogue.length);
			showDialogue(dialogue[rand]);
			petDisplay.setColor(Color.BLACK);
			exitText.setColor(Color.GRAY);
			exitText.setTouchable(Touchable.disabled);
		} else {
			showDialogue("hello! this is the hospital.the place where you can heal your wounded pet.");
		}
		
		initScreen();
	}
	
	private void addHealthBar() {
		DisplayObject playerWindow = createWindow("window6", 255,338, 300, 90);
		Pet pet = Core.getInstance().player.getPet();
		DisplayText nameAndLevel = createText(pet.name+" lv."+pet.level,20,50,font);
		DisplayObject playerGauge = createGauge(pet,20,14);
		playerWindow.addActor(nameAndLevel);
		playerWindow.addActor(playerGauge);
		addActor(playerWindow);
	}
	
	private DisplayObject createGauge(Pet pet,float x,float y) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion gaugeTexture = atlas.findRegion("gauge");
		Gauge gauge = new Gauge(gaugeTexture,pet,260,20);
		gauge.setPosition(x, y);
		
		return gauge;
	}

	private void addPetDisplay() {
		Pet pet = Core.getInstance().player.getPet();
		
		petDisplay = new PetDisplay(pet);
		//petDisplay.setOrigin(petDisplay.getWidth()/2, petDisplay.getHeight()/2);
		petDisplay.setPosition(368,187);
		addActor(petDisplay);
//		petDisplay.addListener(new ActorDragListener());
		
	}

	private void addDialogueWindow() {
		dialogueWindow = createWindow("window6", 223, 14, 560, 150);
		addActor(dialogueWindow);
	}
	
	private void showDialogue(String text) {
		dialogueWindow.clearChildren();
		
		DisplayText textDisplay = createText(text, 0, 0, font);
		textDisplay.setPreferedWidth(dialogueWindow.getWidth() - 40);
		textDisplay.setPosition(20, dialogueWindow.getHeight() - textDisplay.getTextHeight() - 25);
		dialogueWindow.addActor(textDisplay);
	}

	private void addMenuWindow() {
		DisplayObject menuWindow = createWindow("window6", 14,14, 200, 150);
		addActor(menuWindow);
		
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		healText = createText("Heal", 26,95, font);
		healText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Player player = Core.getInstance().player;
				Pet pet = player.getPet();
				int cost=0;
				
//				if(player.getPet().isFaint) {
//					cost = pet.health * 2;
//				} else {
//					cost = pet.health - pet.currentHealth;
//				}
				
				//if(selected == healText){
//					if(cost > player.getGold()) {
//						showDialogue("you don't have enough gold. ok we will take all your gold.\n\n"+pet.name+" is healed!");
//						cost = player.getGold();
//					} else {
						showDialogue(pet.name+" is healed!");
//					}
					if(player.getPet().isFaint) {
						petDisplay.setColor(Color.WHITE);
					}
					
					pet.currentHealth = pet.health;
					pet.isFaint = false;
					player.setGold(player.getGold() - cost);
					
					healText.setColor(Color.GRAY);
					healText.setTouchable(Touchable.disabled);
					
					exitText.setColor(Color.BLACK);
					exitText.setTouchable(Touchable.enabled);
				//} else {
					selected = healText;
					highLight(list, healText);
					
					//showDialogue("healing your pet cost "+cost+" gold.\n\n(tap to heal pet)");
				//}
				
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		
		exitText = createText("Exit", 26, 38, font);
		exitText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				showDialogue("thank you!comeback again.");
				//if(selected == exitText){
					switchScreen(new MapScreen());
				//} else {
					selected = exitText;
					highLight(list, exitText);
				//}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		
		menuWindow.addActor(healText);
		menuWindow.addActor(exitText);
		list.add(healText);
		list.add(exitText);
	}
	
	private void highLight(ArrayList<DisplayText> list ,DisplayText text) {
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getTouchable() == Touchable.disabled) {
				list.get(i).setColor(Color.GRAY);
			} else {
				list.get(i).setColor(Color.BLACK);
			}
		}
		for(int i=0;i<list.size();i++) {
			if(text == list.get(i)) {
				list.get(i).setColor(Color.BLUE);
			}
		}
	}
	
	private DisplayText createText(String string, float x, float y,
			BitmapFont font) {

		DisplayText textDisplay = new DisplayText(string, font);
		textDisplay.setColor(Color.BLACK);
		textDisplay.setPosition(x, y);
		// textDisplay.isDebug = true;
//		 textDisplay.addListener(new ActorDragListener());
		return textDisplay;
	}
	
	private DisplayObject createWindow(String textureName , float x, float y ,  float width , float height ) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		final NinePatch texture = atlas.createPatch(textureName);
		
		DisplayObject window = new DisplayObject(){
			@Override
			public void draw(Batch batch, float parentAlpha) {
				texture.draw(batch, getX(), getY(), getWidth(), getHeight());
				super.draw(batch, parentAlpha);
			}
		};
		window.setPosition(x, y);
		window.setSize(width, height);
//		window.addListener(new ActorDragListener());
		return window;
	}

}
