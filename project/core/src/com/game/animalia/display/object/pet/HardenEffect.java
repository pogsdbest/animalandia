package com.game.animalia.display.object.pet;

import org.omg.CORBA.portable.RemarshalException;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.game.animalia.Assets;
import com.game.animalia.display.object.TapCallback;
import com.game.animalia.display.object.effects.Effect;
import com.game.animalia.display.object.effects.EffectCallback;
import com.game.animalia.display.screen.BattleScreen;
import com.game.framework.display.DisplayObject;
import com.game.framework.utils.L;

public class HardenEffect extends Effect {
	
	public static final String NAME = "Harden";
	private PetDisplay display;
	private int turnRemaining;
	public DisplayObject iconDisplay;
	private int originalDefense;
	private int level;
	
	
	public HardenEffect(PetDisplay display,int originalDefense,int level) {
		super(NAME);
		this.display = display;
		this.originalDefense = originalDefense;
		this.level = level;
		turnRemaining = 3;
		addActor(getDisplayObject());
	}
	
	public DisplayObject getDisplayObject() {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion fear = atlas.findRegion("defense");
		iconDisplay = new DisplayObject(fear);
		return iconDisplay;
	}
	
	@Override
	public void activateEffect(final BattleScreen screen,final EffectCallback callback) {

		turnRemaining -= 1;
		L.wtf("harden turn remaining "+turnRemaining);
		Pet pet = display.getPet();
		if(turnRemaining <= 0) {
			pet.defense = originalDefense;
			remove();
			display.getEffects().remove(this);
			screen.showDialogue(pet.name+" Harden worn out.", new TapCallback() {
				
				@Override
				public void tap() {
					callback.callback();
				}
			});
		} else {
			pet.defense = originalDefense + (5 + (level * 2));
			callback.callback();
		}
	}

}
