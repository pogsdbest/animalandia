package com.game.animalia.util;

public class BattleInfo {

	private static BattleInfo instance;
	
	public int rankID;

	public static BattleInfo getInstance() {
		if (instance == null) {
			instance = new BattleInfo();
		}
		return instance;
	}

	public String getRank(int id) {
		switch (id) {
		case 1:
			return "Newbie";
			
		case 2:
			return "Amateur";
			
		case 3:
			return "Warrior";
			
		case 4:
			return "Champion";
			

		default:
			break;
		}
		return "no rank";
	}

}
