package com.game.animalia;

import com.badlogic.gdx.Game;
import com.game.animalia.display.screen.LoadingScreen;
import com.game.animalia.util.Core;
import com.game.framework.manager.ScreenManager;

public class AnimaliaGame extends Game {
	
	@Override
	public void create() {
		Assets.getInstance().init();
		ScreenManager.getInstance().init(this);
		
		ScreenManager.getInstance().setScreen(new LoadingScreen());
	}
	
	@Override
	public void dispose() {
		
		super.dispose();
	}
	
	@Override
	public void pause() {
		
		super.pause();
	}
}
