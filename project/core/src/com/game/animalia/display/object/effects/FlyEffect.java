package com.game.animalia.display.object.effects;

import java.util.Random;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Cubic;

import com.game.animalia.display.object.TapCallback;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.animalia.display.screen.BattleScreen;
import com.game.animalia.network.NetworkData;
import com.game.framework.utils.DisplayObjectTweenAccessor;
import com.game.framework.utils.L;

public class FlyEffect extends Effect {
	
	public static final String NAME = "fly";
	public float chanceToLand = .5f;
	private PetDisplay display;
	private int flyLevel;
	private boolean isDone = false;
	private float originalY;
	
	public FlyEffect(PetDisplay display,int flyLevel,float originalY) {
		super(NAME);
		this.display = display;
		this.flyLevel = flyLevel;
		this.originalY = originalY;
		
	}
	
	@Override
	public void activateEffect(final BattleScreen screen,final EffectCallback callback) {
		Pet pet = display.getPet();
		int healthRegen = 5 + (int)(flyLevel * 2);
		isDone = false;
		float randomChance = (float)Math.random() + chanceToLand;
		if(NetworkData.getInstance().isClient || NetworkData.getInstance().isServer) {
			int additionalSeed = 0;
			if(NetworkData.getInstance().isServer) {
				if(display == screen.playerPetDisplay) additionalSeed = 10;
				else additionalSeed = 11;
			}
			if(NetworkData.getInstance().isClient) {
				if(display == screen.playerPetDisplay) additionalSeed = 11;
				else additionalSeed = 10;
			}
			
			Random random = new Random((long)screen.randomSeed + additionalSeed);
			randomChance = (random.nextInt(100) * .01f) + chanceToLand;
			
			if(NetworkData.getInstance().isClient)
				L.wtf("client randomChance "+randomChance);
			else L.wtf("server randomChance "+randomChance);
		}
		if(randomChance > 1) {
			display.getEffects().remove(this);
			
			Tween.to(display, DisplayObjectTweenAccessor.POSITION_Y, .3f)
		    .target(originalY)
		    .ease(Cubic.IN)
		    .start(display.getTweenManager())
		    .setCallback(new TweenCallback() {
				
				@Override
				public void onEvent(int arg0, BaseTween<?> arg1) {
					isDone = true;
				}
			});
			screen.showDialogue(pet.name+" landed!", new TapCallback() {
				
				@Override
				public void tap() {
					//if(isDone) {
						callback.callback();
					//}
				}
			});
		} else {
			pet.currentHealth += healthRegen;
			if(pet.currentHealth > pet.health) pet.currentHealth = pet.health;
			screen.showDialogue(pet.name+" regenerates "+healthRegen+" health while flying!", new TapCallback() {
				
				@Override
				public void tap() {
					callback.callback();
				}
			});
		}
	}

}
