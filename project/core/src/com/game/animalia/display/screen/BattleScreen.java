package com.game.animalia.display.screen;

import java.util.ArrayList;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Cubic;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;
import com.game.animalia.Assets;
import com.game.animalia.Config;
import com.game.animalia.display.object.ActionFinishedCallback;
import com.game.animalia.display.object.Gauge;
import com.game.animalia.display.object.TapCallback;
import com.game.animalia.display.object.Turn;
import com.game.animalia.display.object.Turn.TurnType;
import com.game.animalia.display.object.effects.Effect;
import com.game.animalia.display.object.effects.EffectCallback;
import com.game.animalia.display.object.effects.FearEffect;
import com.game.animalia.display.object.effects.FlyEffect;
import com.game.animalia.display.object.effects.NineLivesEffect;
import com.game.animalia.display.object.items.Item;
import com.game.animalia.display.object.items.PetFood;
import com.game.animalia.display.object.pet.HardenEffect;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.animalia.display.object.pet.PetGenerator;
import com.game.animalia.display.object.skills.PetSkill;
import com.game.animalia.network.GameRequest;
import com.game.animalia.network.NetworkData;
import com.game.animalia.network.NetworkInterface;
import com.game.animalia.network.PlayerData;
import com.game.animalia.network.RequestType;
import com.game.animalia.network.TurnData;
import com.game.animalia.util.BattleInfo;
import com.game.animalia.util.Core;
import com.game.animalia.util.Player;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.utils.DisplayObjectTweenAccessor;
import com.game.framework.utils.L;
import com.sun.corba.se.spi.legacy.connection.GetEndPointInfoAgainException;

public class BattleScreen extends AbstractScreen implements NetworkInterface {
	
	private DisplayObject dialogueWindow;
	private DisplayObject listWindow;
	private DisplayObject commandWindow;
	private DisplayObject playerWindow;
	private DisplayObject foeWindow;
	
	public PetDisplay playerPetDisplay;
	public PetDisplay foePetDisplay;
	
	private DisplayText selectedItem;
	
	private Turn playerTurn = new Turn();
	private Turn foeTurn = new Turn();
	
	private int tutorialState = 0;

	private DisplayText fightText;

	private DisplayText itemText;

	private DisplayText runText;
	
	private boolean isClientReady;
	public float randomSeed;

	public BattleScreen() {
		super();
		Assets.getInstance().playMusic("sfx/battle.mp3");
		
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion bg = atlas.findRegion("battlescreenbg");
		DisplayObject bgDisplay = new DisplayObject(bg);
		addActor(bgDisplay);
		
		//createPetFoeForTesting(1);
		
		createPetDisplay();
		createHealthBar();

		createDialogueWindow();
		createCommandWindow();
		createListWindow();
		
		if(Core.getInstance().isTutorial){
			tutorialState = 1;
			showTutorialDialogue();
		} else {
			if(NetworkData.getInstance().isClient || NetworkData.getInstance().isServer) {
				Player foe = Core.getInstance().foe.get(0);
				NetworkData.getInstance().connection.setNetworkInterface(this);
				battleStart("player "+foe.getName()+" wants to fight you!!");
			} else {
				battleStart("a wild "+foePetDisplay.getPet().name+" appeared! what should we do?");
			}
		}
		
		initScreen();
	}
	
	private void createPetFoeForTesting(int id) {
		Core.getInstance().load();
		Player foe = new Player();
		Pet pet = PetGenerator.getInstance().getPet(id);
		pet.getSkills().remove(1);
		foe.setPet(pet);
		Core.getInstance().foe.add(0,foe);
	}

	private void showTutorialDialogue() {
		commandWindow.setVisible(false);
		listWindow.setVisible(false);
		if(tutorialState == 1) {
			tutorialState = 2;
			showDialogue("This is the battle screen. here you will command your pet on the left side to defeat your foe pet on the right side."
			, new TapCallback() {
				
				@Override
				public void tap() {
					showDialogue("the one who reach health points to 0 will lose. and the last pet standing will won the match. ok "+
				Core.getInstance().player.getName()+" lets start the battle.", new TapCallback() {
					
					@Override
					public void tap() {
						PetSkill skill = Core.getInstance().player.getPet().getSkills().get(0);
						itemText.setColor(Color.GRAY);
						itemText.setTouchable(Touchable.disabled);
						runText.setColor(Color.GRAY);
						runText.setTouchable(Touchable.disabled);
						battleStart("tap Fight twice and tap "+skill.name+" twice.");
						
					}
				});
				}
			});
		}else if(tutorialState == 2) {
			tutorialState = 3;
			Pet pet = playerPetDisplay.getPet();
			showDialogue("you can also use "+pet.name+"'s special skill.use it to gain advantage to other pets.", new TapCallback() {
				
				@Override
				public void tap() {
					PetSkill skill = Core.getInstance().player.getPet().getSkills().get(0);
					battleStart("tap Fight twice and tap "+skill.name+" twice.");
				}
			});
		} else if(tutorialState == 3) {
			tutorialState = 4;
			Pet pet = playerPetDisplay.getPet();
			showDialogue(pet.name+"'s hurt and we need to treat our pet to make them survive.let me give you a usable item to restore your pet health points.", new TapCallback() {
				
				@Override
				public void tap() {
					Player player = Core.getInstance().player;
					player.addItem(new PetFood());
					itemText.setColor(Color.BLACK);
					itemText.setTouchable(Touchable.enabled);
					fightText.setColor(Color.GRAY);
					fightText.setTouchable(Touchable.disabled);
					battleStart("tap item twice and tap Pet Food twice.");
				}
			});
		} else if(tutorialState == 4) {
			tutorialState = 5;
			Pet pet = playerPetDisplay.getPet();
			showDialogue("now that "+pet.name+" is healed. let's finish this battle!", new TapCallback() {
				
				@Override
				public void tap() {
					fightText.setColor(Color.BLACK);
					fightText.setTouchable(Touchable.enabled);
					battleStart("do the finishing blow!");
				}
			});
		} else if(tutorialState >= 5) {
			battleStart("do the finishing blow!");
		}
	}

	private void createPetDisplay() {
		Pet pet = Core.getInstance().player.getPet();
		playerPetDisplay = new PetDisplay(pet);
		playerPetDisplay.setFlipX(true);
		playerPetDisplay.setPosition(110, 200);
		playerPetDisplay.setTwenManager(manager);
//		playerPetDisplay.addListener(new ActorDragListener());
		addActor(playerPetDisplay);
		
		Pet foePet = Core.getInstance().foe.get(0).getPet();
		foePetDisplay = new PetDisplay(foePet);
		foePetDisplay.setPosition(570, 200);
		foePetDisplay.setTwenManager(manager);
//		foePetDisplay.addListener(new ActorDragListener());
		addActor(foePetDisplay);
		L.wtf("attack "+foePet.attack);
		L.wtf("defense "+foePet.defense);
		L.wtf("speed "+foePet.speed);
		L.wtf("accuraccy "+foePet.accuracy);
		
	}

	private void battleStart(String detail) {
		ShowDetails(detail);
		commandWindow.setVisible(true);
		dialogueWindow.setVisible(false);
		if(NetworkData.getInstance().isClient || NetworkData.getInstance().isServer) {
			
			NetworkData.getInstance().state = NetworkData.WAIT_FOR_TURN;
			commandWindow.addAction(new Action() {
				
				@Override
				public boolean act(float delta) {
					if(NetworkData.getInstance().state == NetworkData.INTERPRET_TURN){
						readyToInterpretTurn();
						
						return true;
					}
					return false;
				}
			});
		}
	}

	private void createDialogueWindow() {
		dialogueWindow = createWindow("window1",25,10);
		
		dialogueWindow.setVisible(false);
	}

	private void createListWindow() {
		listWindow = createWindow("window3", 255,10);
		
		listWindow.setVisible(false);
	}

	private void createList(DisplayObject listWindow ,ArrayList<DisplayText> list) {
		for(int i=0; i < list.size();i++) {
			DisplayText listItem = list.get(i);
			float x = ((i%2) * (listWindow.getWidth()/2) ) + 25;
			float y =  listWindow.getHeight() - 60 - ((i/2) * 50);
			listItem.setPreferedWidth(230);
//			listItem.isDebug = true;
			listItem.setAlignment(HAlignment.LEFT);
			listItem.setPosition(x ,y);
			listWindow.addActor(listItem);
		}
		
	}

	private void createCommandWindow() {
		commandWindow = createWindow("window2", 25,10);
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		
		fightText = createText("Fight", 23,118, 20);
		fightText.setPreferredWidthInBounds(commandWindow.getWidth()-46);
		fightText.autoResize = false;
		fightText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				highLight(list, fightText);
				showSkills();
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		itemText = createText("Item", 23, 71, 20);
		if(NetworkData.getInstance().isClient || NetworkData.getInstance().isServer) {
			disableText(itemText);
		}
		itemText.setPreferredWidthInBounds(commandWindow.getWidth()-46);
		itemText.autoResize = false;
		itemText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				highLight(list, itemText);
				showItems();
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		runText = createText("Run", 23, 23, 20);
		runText.setPreferredWidthInBounds(commandWindow.getWidth()-46);
		runText.autoResize = false;
		runText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				highLight(list, runText);
				//ShowDetails("are you sure you want to run?");
				showDialogue("Run coward!!", new TapCallback() {
					
					@Override
					public void tap() {
						Assets.getInstance().stopMusic();
						Core.getInstance().foe.clear();
						switchScreen(new ArenaScreen());
					}
				});
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		if(NetworkData.getInstance().isClient || NetworkData.getInstance().isServer || Core.getInstance().isTutorial) {
			disableText(runText);
		}
		
		commandWindow.addActor(fightText);
		commandWindow.addActor(itemText);
		commandWindow.addActor(runText);
		
		list.add(fightText);
		list.add(itemText);
		list.add(runText);
	}
	
	private void highLight(ArrayList<DisplayText> list ,DisplayText text) {
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getTouchable() == Touchable.disabled) continue;
			list.get(i).setColor(Color.BLACK);
		}
		for(int i=0;i<list.size();i++) {
			if(text == list.get(i)) {
				list.get(i).setColor(Color.BLUE);
			}
		}
	}

	private void createHealthBar() {
		playerWindow = createWindow("window4",25,380);
		Pet playerPet = playerPetDisplay.getPet();
		DisplayText nameAndLevel = createText(playerPet.name+" lv."+playerPet.level,20,50,20);
		DisplayObject playerGauge = createGauge(playerPet,20,14);
		playerWindow.addActor(nameAndLevel);
		playerWindow.addActor(playerGauge);
		
		foeWindow = createWindow("window4",475,380);
		Pet foePet = foePetDisplay.getPet();
		DisplayText foeNameAndLevel = createText(foePet.name+ " lv."+foePet.level,20,50,20);
		DisplayObject foeGauge = createGauge(foePet,20,14);
		foeWindow.addActor(foeNameAndLevel);
		foeWindow.addActor(foeGauge);
		
		if(NetworkData.getInstance().isClient || NetworkData.getInstance().isServer) {
			playerPet.currentHealth = playerPet.health;
		}
	}
	
	private DisplayObject createWindow(String textureName,int x,int y) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion texture = atlas.findRegion(textureName);
		DisplayObject window = new DisplayObject(texture);
		addActor(window);
		window.setPosition(x,y);
		//window.addListener(new ActorDragListener());
		return window;
	}
	
	private DisplayText createText(String string, float x,float y,float size) {
		BitmapFont font = Assets.getInstance().generateFont((int)size, string);
		DisplayText textDisplay = new DisplayText(string,font);
		textDisplay.setColor(Color.BLACK);
		textDisplay.setPosition(x, y);
		//textDisplay.isDebug = true;
		//textDisplay.addListener(new ActorDragListener());
		return textDisplay;
	}
	
	private DisplayObject createGauge(Pet pet,float x,float y) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion gaugeTexture = atlas.findRegion("gauge");
		Gauge gauge = new Gauge(gaugeTexture,pet,250,20);
		gauge.setPosition(x, y);
		
		return gauge;
	}
	
	private void ShowDetails(String text) {
		listWindow.setVisible(true);
		listWindow.clear();
		DisplayText detailText = createText(text, 20, 20, 20);
		detailText.setPreferedWidth(listWindow.getWidth() - 40);
		detailText.setAlignment(HAlignment.LEFT);
		detailText.setPosition(20, listWindow.getHeight() - 20 - detailText.getTextHeight());
		listWindow.addActor(detailText);
//		detailText.isDebug = true;
	}
	
	public void showDialogue(String text,final TapCallback callback) {
		dialogueWindow.clearChildren();
		dialogueWindow.setVisible(true);
		commandWindow.setVisible(false);
		listWindow.setVisible(false);
		
		DisplayText dialogueText = createText(text, 20, 20, 20);
		dialogueText.setPreferedWidth(dialogueWindow.getWidth() - 40);
		dialogueText.setAlignment(HAlignment.LEFT);
		dialogueText.setPosition(20, dialogueWindow.getHeight() - 20 - dialogueText.getTextHeight());
		dialogueWindow.addActor(dialogueText);
		
		
		dialogueWindow.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				dialogueWindow.removeListener(this);
				callback.tap();
				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}
	
	private void showSkills() {
		listWindow.clear();
		ArrayList<PetSkill> list = playerPetDisplay.getPet().getSkills();
		final ArrayList<DisplayText> textList = new ArrayList<DisplayText>();
		
		for(int i =0;i<list.size();i++) {
			final DisplayText displayText = createText(list.get(i).name, 0, 0, 20);
			textList.add(displayText);
			if(Core.getInstance().isTutorial) {
				if(tutorialState == 2) {
					if(i == 1) {
						displayText.setColor(Color.GRAY);
						displayText.setTouchable(Touchable.disabled);
					} else {
						displayText.setColor(Color.BLACK);
						displayText.setTouchable(Touchable.enabled);
					}
				}
				if(tutorialState == 3) {
					if(i == 0) {
						displayText.setColor(Color.GRAY);
						displayText.setTouchable(Touchable.disabled);
					} else {
						displayText.setColor(Color.BLACK);
						displayText.setTouchable(Touchable.enabled);
					}
				}
			}
			
			final PetSkill skill = list.get(i);
			displayText.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					
					//if(selectedItem == displayText){
					selectedItem = displayText;
					highLight(textList, displayText);
					playerTurn.setTurn(TurnType.SKILL, skill);
					turnEnd();
					//} else {
						
					//}
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		}
		createList(listWindow , textList);
		listWindow.setVisible(true);
	}
	
	private void showItems() {
		listWindow.clear();
		ArrayList<Item> items = Core.getInstance().player.getItems();
		final ArrayList<DisplayText> textList = new ArrayList<DisplayText>();
		
		for(int i=0;i<items.size();i++) {
			final DisplayText displayText = createText(items.get(i).name + " x"+items.get(i).quantity, 0, 0, 20);
			textList.add(displayText);
			final Item item = items.get(i);
			displayText.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					//if(selectedItem == displayText){
					selectedItem = displayText;
					highLight(textList, displayText);
					playerTurn.setTurn(TurnType.ITEM, item);
					turnEnd();
					//} else {
						
					//}
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		}
		
		createList(listWindow , textList);
		listWindow.setVisible(true);
	}
	
	private void turnEnd() {
		foeTurn();
		
	}
	
	private void foeTurn() {
		if(NetworkData.getInstance().isClient || NetworkData.getInstance().isServer){
			if(NetworkData.getInstance().isClient) {
				sendInfoIntoServer();
			}else if(NetworkData.getInstance().isServer) {
				waitForClientRequest();
			}
			showWaitingForBothTurns();
		} else {
			Pet foePet = foePetDisplay.getPet();
			ArrayList<PetSkill> foeSkills = foePet.getSkills();
			int random = (int)(Math.random() * foeSkills.size());
			
			PetSkill randomSkill = foeSkills.get(random);
			foeTurn.setTurn(TurnType.SKILL, randomSkill);
			
			interpretTurn();
		}
		
	}
	
	private void showWaitingForBothTurns() {
		showDialogue("waiting for other players turn!",new TapCallback() {
			@Override
			public void tap() {
				
			}
			
		});
	}

	private void interpretTurn() {
		if(playerTurn.getTurnType() == TurnType.ITEM) {
			turnItem(playerPetDisplay , playerPetDisplay.getPet() , playerTurn.getItem(),new TapCallback() {
				@Override
				public void tap() {
					
					final PetSkill foeSkill = foeTurn.getPetSkill();
					turnSkill(foePetDisplay,playerPetDisplay, foePetDisplay.getPet(), foeSkill, new TapCallback() {
						@Override
						public void tap() {
							if(Core.getInstance().isTutorial) {
								
								showTutorialDialogue();
							} else {
								battleStart(playerPetDisplay.getPet().name+" is now ready!!!");
							}
						}
					});
				}
			});
		} else if(playerTurn.getTurnType() == TurnType.SKILL){
			final PetSkill playerSkill = playerTurn.getPetSkill();
			final PetSkill foeSkill = foeTurn.getPetSkill();
			
			int playerSkillSpeed = playerSkill.getSpeed(playerPetDisplay.getPet());
			int foeSkillSpeed = foeSkill.getSpeed(foePetDisplay.getPet());
			if(playerSkillSpeed == foeSkillSpeed) {
				if(NetworkData.getInstance().isClient) {
					playerSkillSpeed -= 1;
				} else if(NetworkData.getInstance().isServer) {
					foeSkillSpeed -= 1;
				}
			}
			
			if(playerSkillSpeed >= foeSkillSpeed ) {
				turnSkill(playerPetDisplay,foePetDisplay, playerPetDisplay.getPet(), playerSkill, new TapCallback() {
					
					@Override
					public void tap() {
						turnSkill(foePetDisplay,playerPetDisplay, foePetDisplay.getPet(), foeSkill, new TapCallback() {
							
							@Override
							public void tap() {
								if(Core.getInstance().isTutorial){
									
									showTutorialDialogue();
								} else {
									battleStart(foePetDisplay.getPet().name+" still wants to fight. what should we do?");
								}
							}
						});
					}
				});
			} else {
				turnSkill(foePetDisplay,playerPetDisplay, foePetDisplay.getPet(), foeSkill, new TapCallback() {
					
					@Override
					public void tap() {
//						if(playerPetDisplay.getPet().isFaint) return;
						turnSkill(playerPetDisplay,foePetDisplay, playerPetDisplay.getPet(), playerSkill, new TapCallback() {
							@Override
							public void tap() {
								battleStart(foePetDisplay.getPet().name+" still wants to fight. what should we do?");
							}
						});
					}
				});
			}
		}
	}

	private void turnItem(final PetDisplay display,final Pet pet,final Item item,final TapCallback callback) {
		simulateEffect(display.getEffects(), 0, new TapCallback() {
			@Override
			public void tap() {
				showDialogue("item "+item.name+" has been used on "+pet.name+"!",new TapCallback() {
					
					@Override
					public void tap() {
						item.used();
						if(item.quantity == 0) {
							Core.getInstance().player.getItems().remove(item);
						}
						display.addAction( item.getAction(pet,new ActionFinishedCallback() {
							
							@Override
							public void finished() {
								showDialogue(item.getText(pet), new TapCallback() {
									
									@Override
									public void tap() {
										callback.tap();
									}
								});
							}
						}));
					}
				});
			}
		});
	}
	
	private void turnSkill(final PetDisplay userDisplay,final PetDisplay targetDisplay,final Pet pet,final PetSkill skill,final TapCallback callback) {
		simulateEffect(userDisplay.getEffects(), 0, new TapCallback() {
			
			@Override
			public void tap() {
				if(userDisplay.hasEffect(FearEffect.NAME)) {
					callback.tap();
					return;
				}
				if(userDisplay.hasEffect(FlyEffect.NAME)) {
					L.wtf("user : "+pet.name+" is flying ");
					showDialogue(pet.name+" is still flying!", new TapCallback() {
						
						@Override
						public void tap() {
							callback.tap();
							
						}
					});
					return;
				}
				if(targetDisplay.hasEffect(FlyEffect.NAME) && skill.isHitingFoe){
					L.wtf("user : "+pet.name+" skill : "+skill.name+"  - target is flying ");
					showDialogue(pet.name+" can't reach "+targetDisplay.getPet().name, new TapCallback() {
						
						@Override
						public void tap() {
							callback.tap();
							
						}
					});
					return;
				}
				
				
				showDialogue(pet.name+" used "+skill.name+"!", new TapCallback() {
					
					@Override
					public void tap() {
						userDisplay.addAction(skill.getAction(userDisplay,new ActionFinishedCallback() {
							String text = "";
							int damage;
							@Override
							public void finished() {
								
								boolean isMissed = false;
								boolean hasEffect = false;
								damage = skill.getDamage(pet) - targetDisplay.getPet().defense;
								if(damage <= 0) damage = 1;
								int hitRate = skill.getHitrate(pet);
								if(skill.isDodgeable) {
									float hitChance = (float)hitRate / (float)(targetDisplay.getPet().speed);
									float randomMiss = (float)(Math.random()) + hitChance;
									if(NetworkData.getInstance().isClient || NetworkData.getInstance().isServer) {
										randomMiss = skill.randomMiss + hitChance;
									}
									if(randomMiss < 1.0) {
										text = pet.name+" missed.";
										isMissed = true;
									}
								} else {
									float hitChance = (float)hitRate * .01f;
									float randomEffectSuccess = (float)Math.random() + hitChance;
									if(NetworkData.getInstance().isClient || NetworkData.getInstance().isServer) {
										randomEffectSuccess = skill.randomSuccess + hitChance;
									}
									L.wtf("skill effect "+skill.name+" chance = "+randomEffectSuccess);
									if(randomEffectSuccess > 1) {
										hasEffect = true;
									} else {
										text = skill.getEffectText(userDisplay.getPet(), targetDisplay.getPet() , false);
										isMissed = true;
									}
								}
								
								if(!isMissed) {
									if(hasEffect) {
										text = skill.getEffectText(userDisplay.getPet(), targetDisplay.getPet() , true);
										skill.activateSkill(userDisplay, targetDisplay);
									} else {
										if(skill.getDamage(userDisplay.getPet()) > 0) {
											int totalDamage = targetDisplay.getPet().damage(damage);
											text = targetDisplay.getPet().name+" lost "+totalDamage+" health points.";
											
											if(targetDisplay.getPet().isFaint) {
												if(targetDisplay.hasEffect(NineLivesEffect.NAME)) {
													if(targetDisplay.getPet().currentHealth == 1){
														targetDisplay.getPet().isFaint = true;
														totalDamage = 1;
														targetDisplay.getPet().currentHealth = 0;
													} else {
														targetDisplay.getPet().isFaint = false;
														totalDamage = totalDamage - 1;
														targetDisplay.getPet().currentHealth = 1;
													}
													
													text = targetDisplay.getPet().name+" lost "+totalDamage+" health points.";
												}
											}
											
										}
										
										
									}
									
									
									
									if(targetDisplay.getPet().isFaint) {
										
										text = targetDisplay.getPet().name+" was knock out!!!";
										targetDisplay.getEffects().clear();
										targetDisplay.clearChildren();
									}
									targetDisplay.addAction(skill.getDamageAndEffect(userDisplay,targetDisplay,new ActionFinishedCallback() {
										@Override
										public void finished() {
											showDialogue(text, new TapCallback() {
												
												@Override
												public void tap() {
													if(targetDisplay.getPet().isFaint){
														endBattle();
													} else {
														callback.tap();
													}
												}
											});
										}
									}));
									
								} else {
									showDialogue(text, new TapCallback() {
										
										@Override
										public void tap() {
											callback.tap();
										}
									});
								}
							}
						}));
					}
				});
			}
		});
		
		
	}
	
	private void simulateEffect(final ArrayList<Effect> effects,final int index,final TapCallback callback) {
		if(index >= effects.size()) {
			callback.tap();
			return;
		}
		Effect effect = effects.get(index);
		effect.activateEffect(this ,new EffectCallback() {
			
			@Override
			public void callback() {
				simulateEffect(effects, index+1, callback);
			}
		});
		
	}
	
	private void restartBattle() {
		Pet playerPet = Core.getInstance().player.getPet(); 
		playerPet.currentHealth = playerPet.health;
		playerPet.isFaint = false;
				
		Core.getInstance().foe.clear();
		Assets.getInstance().stopMusic();
		Player foe = new Player();
		int rand = (int)(Math.random() * 4) + 1;
		Pet pet = PetGenerator.getInstance().getPet(rand);
		pet.health = 20;
		pet.currentHealth = pet.health;
		pet.speed = 0;
		pet.accuracy = 99;
		pet.getSkills().remove(1);
		foe.setPet(pet);
		Core.getInstance().foe.add(foe);
		switchScreen(new BattleScreen());
	}
	
	private void endBattle() {
		PetDisplay faint;
		if(playerPetDisplay.getPet().isFaint) faint = playerPetDisplay;
		else faint = foePetDisplay;
		
		Tween.to(faint, DisplayObjectTweenAccessor.POSITION_Y, .3f)
	    .target(-faint.getHeight())
	    .ease(Cubic.IN)
	    .start(manager)
	    .setCallback(new TweenCallback() {
	    	@Override
	    	public void onEvent(int arg0, BaseTween<?> arg1) {
	    		String text = "";
	    		if(playerPetDisplay.getPet().isFaint) {
	    			text = "You lose.";
	    		} else {
	    			text = "You win";
	    		}
	    		showDialogue(text, new TapCallback() {
	    			
	    			@Override
	    			public void tap() {
	    				final Player player = Core.getInstance().player;
	    				if(player.getPet().isFaint) {//lose
	    					if(Core.getInstance().isTutorial) {
	    						restartBattle();
	    					} else {
	    						Assets.getInstance().stopMusic();
	    						Core.getInstance().foe.clear();
	    						if(NetworkData.getInstance().isServer || NetworkData.getInstance().isClient) {
	    							NetworkData.getInstance().connection.close();
	    						}
	    						switchScreen(new HospitalScreen());
	    					}
    					} else { // won
    						int expGet = (int)(foePetDisplay.getPet().level * Config.EXP_RATE);
    	    				Pet playerPet = Core.getInstance().player.getPet();
    	    				playerPet.currentExp += expGet;
    	    				
    	    				String expText = playerPet.name+" got "+expGet+" Exp.";
    	    				if(playerPet.currentExp >= playerPet.getNextExp()) {
    	    					playerPet.currentExp -= playerPet.getNextExp();
    	    					playerPet.setLevel(playerPet.level + 1);
    	    					expText = playerPet.name+" got "+expGet+" Exp.\n"+playerPet.name+" level up to "+playerPet.level;
    	    				}
    	    				showDialogue(expText, new TapCallback() {
								
								@Override
								public void tap() {
									if(Core.getInstance().isTutorial) {
		    	    					showDialogue("congratulations "+player.getName()+"!! you won on your First Battle. let's head to your home for you to rest.", new TapCallback() {
		    								
		    								@Override
		    								public void tap() {
		    									Assets.getInstance().stopMusic();
		    									Core.getInstance().foe.clear();
		    									switchScreen(new HomeScreen());
		    								}
		    							});
		    	    				} else {
		    	    					ArrayList<Player> foe = Core.getInstance().foe;
		        						foe.remove(0);
		        						if(foe.size() > 0){
		        							
		        							showDialogue("Prepare for the next match!", new TapCallback() {
		    									
		    									@Override
		    									public void tap() {
		    										Assets.getInstance().stopMusic();
		    										switchScreen(new BattleScreen());
		    									}
		    								});
		        							
		        						} else {
		        							
		        							showDialogue("Congratulation!!you won the Tournament!!", new TapCallback() {
		    									
		    									@Override
		    									public void tap() {
		    										int winningGold = Core.getInstance().winningGold;
		    										Core.getInstance().player.setGold(Core.getInstance().player.getGold() + winningGold);
		    										showDialogue("You Received "+winningGold+" Gold!!", new TapCallback() {
														
														@Override
														public void tap() {
															int rankAchievement = BattleInfo.getInstance().rankID;
															if( rankAchievement > player.getRank()) {
																player.setRank(rankAchievement);
																String rankString = BattleInfo.getInstance().getRank(rankAchievement);
																showDialogue("Your Rank is now "+rankString, new TapCallback() {
																	
																	@Override
																	public void tap() {
																		endTournament();
																	}
																});
															}
															else {
																endTournament();
															}
														}
													});
		    										
		    									}
		    								});
		        							
		        						}
		    	    				}
								}
							});
    					}	
	    			}
	    		});
	    	}
	    });
		
	}
	
	private void disableText(DisplayText text){
		text.setColor(Color.GRAY);
		text.setTouchable(Touchable.disabled);
	}
	
	private void endTournament() {
		Assets.getInstance().stopMusic();
		if(NetworkData.getInstance().isServer || NetworkData.getInstance().isClient) {
			NetworkData.getInstance().connection.close();
		}
		Core.getInstance().foe.clear();
		String storyBg = "";
		int rankAchievement = BattleInfo.getInstance().rankID;
		String rankAchievementString = BattleInfo.getInstance().getRank(rankAchievement);
		storyBg = rankAchievementString + "arenastory" + Core.getInstance().player.getPet().id;
		storyBg = storyBg.toLowerCase();
		switchScreen(new StoryboardScreen(storyBg, true));
	}

	@Override
	public void received(Object obj, Connection con) {
		if(NetworkData.getInstance().isServer) {
			if (obj instanceof GameRequest) {
		        GameRequest request = (GameRequest)obj;
		        if(request.type == RequestType.GET_PLAYER_TURN) {
		        	setFoeTurn(request.foeTurnData);
		        	
		        	isClientReady = true;
		        }
		    }
		} else if(NetworkData.getInstance().isClient) {
			if (obj instanceof GameRequest) {
				GameRequest request = (GameRequest)obj;
		        if(request.type == RequestType.GET_PLAYER_TURN) {
		        	setFoeTurn(request.foeTurnData);
		        	
		        	this.randomSeed = request.randomSeed;
		        	setServerDecision(request.playerTurnData);
		        	NetworkData.getInstance().state = NetworkData.INTERPRET_TURN;
		        }
		    }
		}
	}
	
	private void setServerDecision(TurnData data) {
		playerTurn.getPetSkill().randomMiss = data._randomMiss;
		playerTurn.getPetSkill().randomSuccess = data._randomSuccess;
	}
	
	private GameRequest sendServerResponse() {
		randomSeed = System.currentTimeMillis();
		
		GameRequest response = new GameRequest();
		response.type = RequestType.GET_PLAYER_TURN;
		response.randomSeed = this.randomSeed;
		
		TurnData foeData = new TurnData();
		foeData._randomMiss = (float)(Math.random());
		foeData._randomSuccess = (float)(Math.random());
		foeData.skillName = playerTurn.getPetSkill().name;
		response.foeTurnData = foeData;
		playerTurn.getPetSkill().randomMiss = foeData._randomMiss;
		playerTurn.getPetSkill().randomSuccess = foeData._randomSuccess;
		
		TurnData playerData = new TurnData();
		playerData._randomMiss = (float)(Math.random());
		playerData._randomSuccess = (float)(Math.random());
		response.playerTurnData = playerData;
		foeTurn.getPetSkill().randomMiss = playerData._randomMiss;
		foeTurn.getPetSkill().randomSuccess = playerData._randomSuccess;
		return response;
	}
	
	private void setFoeTurn(TurnData data) {
		Pet foePet = foePetDisplay.getPet();
		ArrayList<PetSkill> foeSkills = foePet.getSkills();
		PetSkill skill = null;
		for(int i=0;i<foeSkills.size();i++) {
			if(foeSkills.get(i).name.equalsIgnoreCase(data.skillName)){
				skill = foeSkills.get(i);
				break;
			}
		}
		skill.randomMiss = data._randomMiss;
		skill.randomSuccess = data._randomSuccess;
		foeTurn.setTurn(TurnType.SKILL, skill);
	}
	
	public void sendInfoIntoServer() {
		GameRequest request = new GameRequest();
		request.type = RequestType.GET_PLAYER_TURN;
		
		TurnData data = new TurnData();
		data.skillName = playerTurn.getPetSkill().name;
		request.foeTurnData = data;
		
		NetworkData.getInstance().connection.sendTCP(request);
	}
	
	public void waitForClientRequest() {
		commandWindow.addAction(new Action() {
			
			@Override
			public boolean act(float delta) {
				if(isClientReady){
					sendDataToClient();
					isClientReady = false;
					return true;
				}
				return false;
			}
		});
	}

	protected void sendDataToClient() {
		GameRequest response = sendServerResponse();
    	NetworkData.getInstance().connection.sendTCP(response);
    	NetworkData.getInstance().state = NetworkData.INTERPRET_TURN;
	}

	@Override
	public void disconnected(Connection con) {
		
	}
	
	public void connected(Connection con) {
		
	}
	
	public void readyToInterpretTurn() {
		interpretTurn();
	}

	@Override
	public void connected(Connection con, Server server) {
		connected(con);
	}

	@Override
	public void connected(Connection con, Client client) {
		connected(con);
	}
}
