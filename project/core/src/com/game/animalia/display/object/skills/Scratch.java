package com.game.animalia.display.object.skills;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Bounce;
import aurelienribon.tweenengine.equations.Cubic;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.game.animalia.display.object.ActionFinishedCallback;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.framework.utils.DisplayObjectTweenAccessor;
import com.game.framework.utils.L;

public class Scratch extends PetSkill {
	
	public boolean isDone = false;

	public Scratch() {
		super("Scratch");
		desc = "attack the foe using sharp claw.";
		isHitingFoe = true;
	}

	@Override
	public Action getAction(PetDisplay display,final ActionFinishedCallback callback) {
		isDone = false;
		float bounceIn  = 0;
		if(display.isFlipX()) bounceIn = display.getX() + 50;
		else bounceIn = display.getX() - 50;
		Tween.to(display, DisplayObjectTweenAccessor.POSITION_X, .3f)
	    .target(bounceIn,0)
	    .ease(Cubic.IN)
	    .repeatYoyo(1, 0f)
	    .start(display.getTweenManager())
	    .setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				isDone = true;
			}
		});
		
		Action action = new Action() {
			@Override
			public boolean act(float delta) {
				if(isDone) {
					callback.finished();
					return true;
				}
				return false;
			}
		};
		return action;
	}

	@Override
	public int getSpeed(Pet pet) {
		
		return (int)(pet.speed * 1.5);
	}

	@Override
	public int getDamage(Pet pet) {
		
		return pet.attack * 1 + (level * 5);
	}

	@Override
	public int getHitrate(Pet pet) {
		
		return (int)(pet.accuracy *.7);
	}

	@Override
	public Action getDamageAndEffect(PetDisplay user, PetDisplay target ,final ActionFinishedCallback callback) {
		
		float bounceIn  = 0;
		if(target.isFlipX()) bounceIn = target.getX() - 50;
		else bounceIn = target.getX() + 50;
		Tween.to(target, DisplayObjectTweenAccessor.POSITION_X, .3f)
	    .target(bounceIn,0)
	    .ease(Cubic.IN)
	    .repeatYoyo(1, 0f)
	    .start(target.getTweenManager())
	    .setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				isDone = true;
			}
		});
		
		Action action = new Action() {
			@Override
			public boolean act(float delta) {
				if(isDone) {
					callback.finished();
					return true;
				}
				return false;
			}
		};
		return action;
	}

	@Override
	public String getEffectText(Pet pet, Pet target,boolean succeed) {
		return null;
	}

	@Override
	public void activateSkill(PetDisplay user, PetDisplay target) {
	}

	
	
	
}
