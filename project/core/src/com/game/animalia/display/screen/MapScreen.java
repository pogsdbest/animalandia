package com.game.animalia.display.screen;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.game.animalia.Assets;
import com.game.animalia.util.Core;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.listeners.ActorDragListener;
import com.game.framework.manager.ScreenManager;
import com.game.framework.utils.L;

public class MapScreen extends AbstractScreen {
	
	public MapScreen() {
		super();
		
		Assets.getInstance().playMusic("sfx/map.mp3");
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion bg = atlas.findRegion("mapbg");
		DisplayObject mapBgDisplay = new DisplayObject(bg);
		addActor(mapBgDisplay);
		
		createArea("homeb","HOME",318,174,new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Assets.getInstance().stopMusic();
				switchScreen(new HomeScreen());
				L.wtf("clicked");
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		createArea("arenab","ARENA",563,212,new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Assets.getInstance().stopMusic();
				switchScreen(new ArenaScreen());
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		createArea("hospitalb","HOSPITAL",477,233,new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Assets.getInstance().stopMusic();
				switchScreen(new HospitalScreen());
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		createArea("shopb","SHOP",136,268,new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Assets.getInstance().stopMusic();
				switchScreen(new ShopScreen());
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		createArea("academyb","ACADEMY",21,127,new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Assets.getInstance().stopMusic();
				switchScreen(new AcademyScreen());
				
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		
		initScreen();
		
	}
	
	private void createArea(String textureName,String name ,int x,int y,InputListener listener) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion img = atlas.findRegion(textureName);
		DisplayObject display = new DisplayObject(img);
		//display.addListener(listener);
		display.setPosition(x, y);
		addActor(display);
//		display.addListener(new ActorDragListener());
		
		BitmapFont font = Assets.getInstance().generateFont(15, name);
		final DisplayText textDisplay = new DisplayText(name,font);
		textDisplay.setColor(Color.BLACK);
		textDisplay.setPreferedWidth(150);
		textDisplay.setY(display.getHeight());
		textDisplay.setX((display.getWidth() - 150)/2);
		textDisplay.setAlignment(HAlignment.CENTER);
		textDisplay.setTouchable(Touchable.disabled);
		display.addActor(textDisplay);
		
		display.addListener(listener);
	}
	
	

}
