package com.game.animalia.network;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;

public class MultiplayerConnection implements NetworkInterface {
	
	private NetworkInterface net;
	private Connection con;
	private Server server;
	private Client client;

	public void setNetworkInterface(NetworkInterface net) {
		this.net = net;
	}
	
	@Override
	public void received(Object obj,Connection con) {
		if(net!=null){
			net.received(obj,con);
		}
	}

	@Override
	public void disconnected(Connection con) {
		if(net!=null) {
			net.disconnected(con);
		}
	}

	@Override
	public void connected(Connection con,Server server) {
		this.con = con;
		this.server = server;
		if(net!=null) {
			net.connected(con,server);
		}
	}
	
	@Override
	public void connected(Connection con,Client client) {
		this.con = con;
		this.client = client;
		if(net!=null) {
			net.connected(con,client);
		}
	}
	
	public void sendTCP(Object object) {
		con.sendTCP(object);
	}
	
	public void close() {
		if(server!=null) {
			con.close();
			server.stop();
		}
		if(client!=null) {
			con.close();
			client.stop();
		}
	}

}
