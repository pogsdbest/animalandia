package com.game.framework.utils;

import com.game.framework.listeners.BackKeyListener;

public class BackKeyCatcher {

	private static BackKeyCatcher instance;
	public BackKeyListener listener;
	
	public static BackKeyCatcher getInstance() {
		if(instance==null) {
			instance = new BackKeyCatcher();
		}
		return instance;
		
	}
	
	public void backPressed() {
		if(listener!=null) {
			listener.backPressed();
			L.wtf("back Key is pressed");
		} else {
			L.wtf("Listener is Null");
		}
		
	}
}
