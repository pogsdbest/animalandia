package com.game.animalia.util;

import java.util.ArrayList;

import com.game.animalia.display.object.items.Item;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.network.PlayerData;

public class Player {
	
	private String name;
	private Pet pet;
	private ArrayList<Item> items = new ArrayList<Item>();
	private int gold;
	private int rank = 1;
	
	public Player() {
		
	}
	
	/**
	 * @return the pet
	 */
	public Pet getPet() {
		return pet;
	}
	/**
	 * @param pet the pet to set
	 */
	public void setPet(Pet pet) {
		this.pet = pet;
	}
	/**
	 * @return the items
	 */
	public ArrayList<Item> getItems() {
		return items;
	}
	/**
	 * @param items the items to set
	 */
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	
	public void addItem (Item item) {
		for(int i=0;i<items.size();i++) {
			if(items.get(i).name.equals(item.name)) {
				items.get(i).quantity += item.quantity;
				return;
			}
		}
		items.add(item);
	}
	/**
	 * @return the gold
	 */
	public int getGold() {
		return gold;
	}
	/**
	 * @param gold the gold to set
	 */
	public void setGold(int gold) {
		this.gold = gold;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the rank
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * @param rank the rank to set
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}
	
	public PlayerData getPlayerData () {
		PlayerData data = new PlayerData();
		data.name = name;
		data.rank = rank;
		
		return data;
	}

}
