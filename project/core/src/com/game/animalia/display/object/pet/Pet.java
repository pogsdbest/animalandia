package com.game.animalia.display.object.pet;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.game.animalia.display.object.skills.PetSkill;
import com.game.animalia.network.PetData;

public abstract class Pet {
	
	public int id;
	public String name;
	
	public int level;
	public int health;
	public int attack;
	public int defense;
	public int speed;
	public int accuracy;
	
	public int currentHealth;
	public int currentExp;
	public String description;
	
	protected ArrayList<PetSkill> skills = new ArrayList<PetSkill>();
	
	protected TextureRegion display;

	public boolean isFaint;
	

	public Pet(String name) {
		this.name = name;
		description = "";
	}
	
	public TextureRegion getDisplay() {
		return display;
	}
	
	public ArrayList<PetSkill> getSkills() {
		
		return skills;
	}
	
	public void setSkills(ArrayList<PetSkill> skills) {
		this.skills = skills;
	}
	
	public int damage(int damage) {
		int lastHealth = currentHealth;
		currentHealth -= damage;
		int totalDamage = damage;
		if(currentHealth <= 0) {
			isFaint = true;
			totalDamage = lastHealth;
			currentHealth = 0;
		}
		
		return totalDamage; 
	}
	
	public int getNextExp() {
		return level * 10; 
	}
	
	public void setLevel(int level) {
		setBaseStats(level);
		currentHealth = health;
	}
	
	public void setBaseStats() {
		setBaseStats(1);
	}
	
	public abstract void setBaseStats(int level);
	public abstract void setBaseSkills();
	
	public PetData getPetData() {
		PetData data = new PetData();
		data.id = id;
		data.name = name;
		data.health = health;
		data.attack = attack;
		data.defense = defense;
		data.speed = speed;
		data.accuracy = accuracy;
		
		data.skills = new int[2];
		for(int i=0;i<skills.size();i++) {
			data.skills[i] = skills.get(i).level;
		}
		
		return data;
	}
	
	public void applyPetData(PetData data) {
		id = data.id;
		name = data.name;
		health = data.health;
		attack = data.attack;
		defense = data.defense;
		speed = data.speed;
		accuracy = data.accuracy;
		
		for(int i=0;i<skills.size();i++){
			skills.get(i).level = data.skills[i];
		}
	}
	
}
