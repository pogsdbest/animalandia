package com.game.animalia.display.object.pet;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.game.animalia.Assets;
import com.game.animalia.display.object.skills.Moo;
import com.game.animalia.display.object.skills.Tackle;

public class Cow extends Pet {

	public Cow() {
		super("Cow");
		
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		display = atlas.findRegion("cow");
		
		description = "animal used as a source of milk or beef.";
	}
	@Override
	public void setBaseStats(int level) {
		this.level = level;
		health = 70 + level * 6;
		attack = 15 + level * 2;
		defense = 7 + (int)(level * 1.5);
		speed = 5 + level * 1;
		accuracy = 5 + level * 1;
	}

	@Override
	public void setBaseSkills() {
		Tackle tackle = new Tackle();
		skills.add(tackle);
		skills.add(new Moo());
	}

}
