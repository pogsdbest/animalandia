package com.game.animalia.display.object.skills;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.game.animalia.display.object.ActionFinishedCallback;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;

public abstract class PetSkill {
	
	public String name;
	public String desc;
	public int level;
	public boolean isHitingFoe;
	
	/*-- for Networking purpose --*/
	public float randomMiss;
	public float randomSuccess;
	
	public boolean isDodgeable;
	public PetSkill(String name) {
		this.name = name;
		level = 1;
		isDodgeable = true;
		isHitingFoe = false; 
	}
	
	public abstract Action getAction(PetDisplay display,ActionFinishedCallback callback);
	public abstract int getSpeed(Pet pet);
	public abstract int getDamage(Pet pet);
	public abstract int getHitrate(Pet pet);
	
	public abstract void activateSkill(PetDisplay user,PetDisplay target);
	public abstract String getEffectText(Pet pet,Pet target,boolean succeed);
	public abstract Action getDamageAndEffect(PetDisplay user, PetDisplay target,ActionFinishedCallback callback);
}
