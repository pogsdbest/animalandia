package com.game.framework.display.ui;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.game.framework.display.DisplayObject;
import com.game.framework.listeners.ActorClickListener;

public class ImageButton extends DisplayObject {
	
	private TextureRegion buttonUp;
	private TextureRegion buttonDown;
	private TextureRegion buttonDisabled;
	private ActorClickListener clickedListener;
	private boolean isEnabled;
	
	public ImageButton(TextureRegion buttonUp,TextureRegion buttonDown) {
		this(buttonUp,buttonDown,null);
	}
	
	public ImageButton(TextureRegion buttonUp,TextureRegion buttonDown,TextureRegion buttonDisabled) {
		super(buttonUp);
		this.buttonUp = buttonUp;
		this.buttonDown = buttonDown;
		this.buttonDisabled = buttonDisabled;
		enabled();
		
		addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// TODO Auto-generated method stub
				super.clicked(event, x, y);
			}
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				pressed();
				return super.touchDown(event, x, y, pointer, button);
			}
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				released();
				super.touchUp(event, x, y, pointer, button);
			}
		});
	}

	protected void released() {
		// TODO Auto-generated method stub
		if(!isEnabled) return;
		setTexture(buttonUp);
		if(this.clickedListener!=null) clickedListener.clicked(this);
	}

	protected void pressed() {
		// TODO Auto-generated method stub
		if(!isEnabled) return;
		setTexture(buttonDown);
	}

	public void setClickedListener(ActorClickListener clickedListener) {
		this.clickedListener = clickedListener;
	}
	
	public void disabled() {
		isEnabled = false;
		if(buttonDisabled!=null) {
			setTexture(buttonDisabled);
		}
	}
	
	public void enabled() {
		isEnabled = true;
		setTexture(buttonUp);
	}
	
}
