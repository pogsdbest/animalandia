package com.game.animalia.display.object.effects;

public interface EffectCallback {

	public void callback();
}
