package com.game.animalia.display.screen;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.game.animalia.Assets;
import com.game.animalia.display.object.items.HealthCapsule;
import com.game.animalia.display.object.items.Item;
import com.game.animalia.display.object.items.PetFood;
import com.game.animalia.util.Core;
import com.game.animalia.util.Player;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.utils.L;

public class ShopScreen extends AbstractScreen {

	private BitmapFont font;
	private DisplayText selected;
	private DisplayObject dialogueWindow;
	private DisplayText quantityText;
	private DisplayObject listWindow;

	public ShopScreen() {
		super();

		font = Assets.getInstance().generateFont(20, Assets.getInstance().charSet);
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion bg = atlas.findRegion("battlescreenbg");
		DisplayObject bgDisplay = new DisplayObject(bg);
		addActor(bgDisplay);

		addMenuWindow();
		addDialogueWindow();
		addListWindow();
		addInfoWindow();
		
		showDialogue("hello! how can i help you?");
		
		initScreen();
	}
	
	

	private void addInfoWindow() {
		DisplayObject infoWindow = createWindow("window6", 14,196, 200, 260);
		addActor(infoWindow);
		
		DisplayText infoText = createText("Shop",20,220, font);
		infoWindow.addActor(infoText);
		
		final Player player = Core.getInstance().player;
		final DisplayText goldText = createText("Gold:\n"+player.getGold(), 20,163, font);
		goldText.setPreferedWidth(infoWindow.getWidth() - 40);
		goldText.addAction(new Action(){
			@Override
			public boolean act(float delta) {
				goldText.setText("Gold:\n"+player.getGold());
				return false;
			}
		});
		infoWindow.addActor(goldText);
		
		quantityText = createText("Have:\n0", 20, 100, font);
		quantityText.setPreferedWidth(infoWindow.getWidth() - 40);
		infoWindow.addActor(quantityText);
		
	}

	private void addListWindow() {
		listWindow = createWindow("window6", 223,196, 560, 260);
		addActor(listWindow);
	}

	private void addDialogueWindow() {
		dialogueWindow = createWindow("window6", 223, 14, 560, 150);
		addActor(dialogueWindow);
	}
	
	private void showDialogue(String text) {
		dialogueWindow.clearChildren();
		
		DisplayText textDisplay = createText(text, 0, 0, font);
		textDisplay.setPreferedWidth(dialogueWindow.getWidth() - 40);
		textDisplay.setPosition(20, dialogueWindow.getHeight() - textDisplay.getTextHeight() - 25);
		dialogueWindow.addActor(textDisplay);
	}

	private void addMenuWindow() {
		DisplayObject menuWindow = createWindow("window6", 14,14, 200, 150);
		addActor(menuWindow);
		
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		final DisplayText buyText = createText("Buy", 45,124, font);
		buyText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				//if(selected == buyText){
					showShopItems();
					showDialogue("double tap to buy item.");
				//}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		final DisplayText sellText = createText("Sell", 45, 78, font);
		sellText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				//if(selected == sellText){
					showPlayerItems();
					showDialogue("double tap to sell item.");
				//}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		final DisplayText exitText = createText("Exit", 45, 33, font);
		exitText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				showDialogue("thank you!comeback again.");
				//if(selected == exitText){
					switchScreen(new MapScreen());
				//}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		
		addActor(buyText);
		addActor(sellText);
		addActor(exitText);
		list.add(buyText);
		list.add(sellText);
		list.add(exitText);
		
		for(int i=0;i<list.size();i++) {
			final DisplayText text = list.get(i);
			text.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					selected = text;
					highLight(list, text);
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		}
	}
	
	protected void showPlayerItems() {
		listWindow.clearChildren();
		DisplayText itemText = createText("Item", 20, 220, font);
		listWindow.addActor(itemText);
		
		ArrayList<Item> playerItems = Core.getInstance().player.getItems();
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		for(int i=0;i<playerItems.size();i++){
			final Item item = playerItems.get(i);
			final DisplayText textDisplay = createText(">"+item.name+" -"+(item.price/2), 0, 0, font);
			list.add(textDisplay);
			textDisplay.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					highLight(list, textDisplay);
					showDialogue(item.desc);
					
					if(selected==textDisplay){
						sell(item);
						updateQuantity(item);
						showPlayerItems();
					} else {
						selected = textDisplay;
						updateQuantity(item);
					}
					
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		}
		createList(listWindow, list);
	}
	
	protected void sell(Item item) {
		Player player = Core.getInstance().player;
		Item playerItem = getPlayerItem(item);
		playerItem.quantity -= 1;
		player.setGold(player.getGold()+playerItem.price/2);
		L.wtf("remaining "+playerItem.quantity);
		if(playerItem.quantity == 0) {
			player.getItems().remove(playerItem);
		}
	}

	protected void showShopItems() {
		listWindow.clearChildren();
		
		DisplayText itemText = createText("Item", 20, 220, font);
		listWindow.addActor(itemText);
		
		ArrayList<Item> items = new ArrayList<Item>();
		items.add(new PetFood());
		items.add(new HealthCapsule());
		
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		for(int i=0;i<items.size();i++) {
			final Item item = items.get(i);
			final DisplayText textDisplay = createText(">"+item.name+" -"+item.price, 0, 0, font);
			list.add(textDisplay);
			textDisplay.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					
					highLight(list, textDisplay);
					showDialogue(item.desc);
					
					if(selected==textDisplay){
						buy(item);
						updateQuantity(item);
						showShopItems();
					} else {
						selected = textDisplay;
						updateQuantity(item);
					}
					
					
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		}
		createList(listWindow, list);
	}
	
	protected void updateQuantity(Item item) {
		Item playerItem = getPlayerItem(item);
		String quantity = "";
		if(playerItem==null) {
			quantity = "Have:\n0";
		} else {
			quantity = "Have:\n"+playerItem.quantity;
		}
		quantityText.setText(quantity);
	}

	protected void buy(Item item) {
		Player player = Core.getInstance().player;
		int playerGold = player.getGold();
		int price = item.price;
		if(playerGold>=item.price) {
			player.setGold(player.getGold() - price);
			L.wtf("new item "+item.quantity);
			player.addItem(item);
		} else {
			showDialogue("you don't have enough gold.");
		}
		selected = null;
	}
	
	private Item getPlayerItem(Item item) {
		ArrayList<Item> items = Core.getInstance().player.getItems();
		for(int i=0;i<items.size();i++){
			if(items.get(i).name.equals(item.name)){
				return items.get(i);
			}
		}
		return null;
	}
	
	private void createList(DisplayObject listWindow ,ArrayList<DisplayText> list) {
		for(int i=0; i < list.size();i++) {
			DisplayText listItem = list.get(i);
			float x = 20;
			float y = listWindow.getHeight() - 80 - (i * 40);
			listItem.setPreferedWidth((listWindow.getWidth()) - 40);
			listItem.setAlignment(HAlignment.LEFT);
			listItem.setPosition(x ,y);
			listWindow.addActor(listItem);
		}
		
	}

	private void highLight(ArrayList<DisplayText> list ,DisplayText text) {
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getTouchable() == Touchable.disabled) {
				list.get(i).setColor(Color.GRAY);
			} else {
				list.get(i).setColor(Color.BLACK);
			}
		}
		for(int i=0;i<list.size();i++) {
			if(text == list.get(i)) {
				list.get(i).setColor(Color.BLUE);
			}
		}
	}

	private DisplayText createText(String string, float x, float y,
			BitmapFont font) {

		DisplayText textDisplay = new DisplayText(string, font);
		textDisplay.setColor(Color.BLACK);
		textDisplay.setPosition(x, y);
		// textDisplay.isDebug = true;
//		 textDisplay.addListener(new ActorDragListener());
		return textDisplay;
	}
	
	private DisplayObject createWindow(String textureName , float x, float y ,  float width , float height ) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		final NinePatch texture = atlas.createPatch(textureName);
		
		DisplayObject window = new DisplayObject(){
			@Override
			public void draw(Batch batch, float parentAlpha) {
				texture.draw(batch, getX(), getY(), getWidth(), getHeight());
				super.draw(batch, parentAlpha);
			}
		};
		window.setPosition(x, y);
		window.setSize(width, height);
//		window.addListener(new ActorDragListener());
		return window;
	}

}
