package com.game.animalia.display.object.skills;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Cubic;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.game.animalia.display.object.ActionFinishedCallback;
import com.game.animalia.display.object.effects.FlyEffect;
import com.game.animalia.display.object.pet.HardenEffect;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.framework.utils.DisplayObjectTweenAccessor;

public class Moo extends PetSkill {
	
	private boolean isDone;

	public Moo() {
		super("Moo");
		desc = "time to release milk.";
		isDodgeable = false;
	}
	
	@Override
	public Action getAction(PetDisplay display,final ActionFinishedCallback callback) {
		isDone = false;
		float bounceIn  = 0;
		if(display.isFlipX()) bounceIn = display.getX() + 50;
		else bounceIn = display.getX() - 50;
		Tween.to(display, DisplayObjectTweenAccessor.POSITION_X, .3f)
	    .target(bounceIn,0)
	    .ease(Cubic.IN)
	    .repeatYoyo(1, 0f)
	    .start(display.getTweenManager())
	    .setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				isDone = true;
			}
		});
		
		Action action = new Action() {
			@Override
			public boolean act(float delta) {
				if(isDone) {
					callback.finished();
					return true;
				}
				return false;
			}
		};
		return action;
	}

	@Override
	public int getSpeed(Pet pet) {
		return 0;
	}

	@Override
	public int getDamage(Pet pet) {
		return 0;
	}

	@Override
	public int getHitrate(Pet pet) {
		return 100;
	}

	@Override
	public Action getDamageAndEffect(PetDisplay user, PetDisplay target ,final ActionFinishedCallback callback) {
		Action action = new Action() {
			@Override
			public boolean act(float delta) {
				
				callback.finished();
				return true;
				
			}
		};
		return action;
	}

	@Override
	public String getEffectText(Pet pet, Pet target,boolean succeed) {
		
		return pet.name+" harden its defense.";
	}

	@Override
	public void activateSkill(PetDisplay user, PetDisplay target) {
		if(user.hasEffect(name)) return;
		HardenEffect effect = new HardenEffect(user,user.getPet().defense,level);
		user.addEffect(effect);
		user.addActor(effect);
		effect.setPosition(user.getWidth()/2,user.getHeight() * .9f );
	}
}
