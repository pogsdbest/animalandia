package com.game.animalia.display.object;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.game.animalia.display.object.pet.Pet;
import com.game.framework.display.DisplayObject;
import com.game.framework.utils.L;

public class Gauge extends DisplayObject {
	
	private Pet pet;
	private Color defaultColor = new Color();
	private int value;

	public Gauge(TextureRegion texture,Pet pet,int width,int height) {
		super(texture);
		this.pet = pet;
		value = pet.currentHealth;
		setWidth(width);
		setHeight(height);
	}
	
	public void setPet(Pet pet) {
		this.pet = pet;
		
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		int currentValue = pet.currentHealth;
		int maxHealth = pet.health;
		
		if(value < currentValue) value += 1;
		else if(value > currentValue) value -= 1;
		if(value == currentValue) value = currentValue;
		
		Color color;
		float fillValue = (float)value / (float)maxHealth;
		if(fillValue > .7) color = Color.GREEN;
		else if(fillValue < .7 && fillValue > .3) color = Color.YELLOW;
		else color = Color.RED;
		
		drawGauge(batch, Color.GRAY,getWidth());
		drawGauge(batch, color,getWidth() * fillValue);
	}
	
	private void drawGauge(Batch batch,Color color,float width) {
		batch.setColor(color);
		batch.draw(getTexture().getTexture(), getX(), getY(), getOriginX(), getOriginY(),
				width, getHeight(), getScaleX(), getScaleY(),
				getRotation(), getTexture().getRegionX(), getTexture().getRegionY(),
				getTexture().getRegionWidth(), getTexture().getRegionHeight(), flipX,
				flipY);
		batch.setColor(defaultColor);
	}

}
