package com.game.animalia.display.object;

public interface ActionFinishedCallback {

	public void finished();
}
