package com.game.animalia.util;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Json;
import com.game.animalia.Config;
import com.game.animalia.display.object.items.Item;
import com.game.animalia.display.object.skills.PetSkill;
import com.game.framework.utils.L;

public class Core {

	private static Core instance;

	public Player player;
	public ArrayList<Player> foe;
	public Setting setting;
	public boolean isTutorial;
	public int winningGold;

	public static Core getInstance() {
		if (instance == null) {
			instance = new Core();
		}
		return instance;
	}
	
	private Core() {
		isTutorial = true;
		setting = new Setting();
		foe = new ArrayList<Player>();
	}

	public void load() {
		Preferences prefs = Gdx.app.getPreferences(Config.PREFERENCE);
		if (prefs.contains("save")) {
			L.wtf("save file detected!");
			Json json = new Json();
			String text = prefs.getString("save");
			Save save = json.fromJson(Save.class, text);
			
			Player player = new Player();
			player.setName(save.getPlayerName());
			
			player.setPet(save.getPet());
			
			player.setGold(save.getGold());
			player.setRank(save.getRank());
			player.setItems(save.getItems());
			player.getPet().setSkills(save.getSkills());
			
			Setting setting = new Setting();
			setting.sound = save.isSound();
			
			isTutorial = save.isTutorial();
			
			this.player = player;
			this.setting = setting;
			
		} else {
			L.wtf("saving file for the first time!");
			save();
		}
	}


	public void save() {
		Preferences prefs = Gdx.app.getPreferences(Config.PREFERENCE);

		Save save = new Save();
		save.setPlayerName(player.getName());
		
		save.setPet(player.getPet());
		
		save.setGold(player.getGold());
		save.setRank(player.getRank());
		ArrayList<Item> items = new ArrayList<Item>();
		save.setItems(player.getItems());
		save.setSkills(player.getPet().getSkills());
		
		save.setSound(setting.sound);
		save.setTutorial(isTutorial);

		Json json = new Json();
		json.setElementType(Save.class, "items", Item.class);
		json.setElementType(Save.class, "skills", PetSkill.class);
		prefs.putString("save", json.toJson(save));
		prefs.flush();
	}
	
	public boolean hasSaved() {
		Preferences prefs = Gdx.app.getPreferences(Config.PREFERENCE);
		return prefs.contains("save");
	}
}
