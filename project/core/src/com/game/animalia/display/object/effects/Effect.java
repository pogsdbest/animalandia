package com.game.animalia.display.object.effects;

import com.game.animalia.display.screen.BattleScreen;
import com.game.framework.display.DisplayObject;

public abstract class Effect extends DisplayObject {
	
	public String name;

	public Effect(String name) {
		this.name = name;
	}
	
	public abstract void activateEffect(BattleScreen screen,EffectCallback callback);
}
