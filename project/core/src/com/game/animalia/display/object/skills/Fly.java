package com.game.animalia.display.object.skills;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.equations.Cubic;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.game.animalia.display.object.ActionFinishedCallback;
import com.game.animalia.display.object.effects.FlyEffect;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.framework.utils.DisplayObjectTweenAccessor;

public class Fly extends PetSkill {
	
	public boolean isDone = false;
	private float originalY;
	
	public Fly() {
		super("Fly");
		desc = "will send you fly high.";
		isDodgeable = false;
	}
	
	@Override
	public Action getAction(PetDisplay display,final ActionFinishedCallback callback) {
		originalY = display.getY();
		isDone = false;
		
		Action action = new Action() {
			@Override
			public boolean act(float delta) {
				
				callback.finished();
				return true;
			}
		};
		return action;
	}

	@Override
	public int getSpeed(Pet pet) {
		return (int)(pet.speed * 2);
	}

	@Override
	public int getDamage(Pet pet) {
		return 0;
	}

	@Override
	public int getHitrate(Pet pet) {
		return 50;
	}

	@Override
	public Action getDamageAndEffect(PetDisplay user, PetDisplay target ,final ActionFinishedCallback callback) {
		Action action = new Action() {
			@Override
			public boolean act(float delta) {
				
				callback.finished();
				return true;
				
			}
		};
		return action;
	}

	@Override
	public String getEffectText(Pet pet, Pet target,boolean succeed) {
		if(succeed) {
			return pet.name+" fly high!";
		}
		return pet.name+" try to fly but fail.";
	}

	@Override
	public void activateSkill(PetDisplay user, PetDisplay target) {
		if(user.hasEffect(name)) return;
		user.addEffect(new FlyEffect(user, level,originalY ));
		float dy = user.getStage().getHeight();
		//L.wtf("dy is "+dy);
		Tween.to(user, DisplayObjectTweenAccessor.POSITION_Y, .3f)
	    .target(dy)
	    .ease(Cubic.IN)
	    .start(user.getTweenManager());
	}

}
