package com.game.animalia.display.object.items;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.game.animalia.display.object.ActionFinishedCallback;
import com.game.animalia.display.object.pet.Pet;

public class HealthCapsule extends Item {
	
	private  int maxHealingValue = 100;
	private int healed;
	
	public HealthCapsule() {
		super("Health Capsule");
		quantity += 1;
		price = 50;
		desc = "restores "+maxHealingValue+" health points.";
	}
	
	@Override
	public String getText(Pet pet) {
		return pet.name+"'s health healed by "+healed;
	}

	@Override
	public Action getAction(final Pet pet,final ActionFinishedCallback callback) {
		int h = pet.currentHealth + maxHealingValue;
		
		if(h > pet.health) {
			this.healed = pet.health - pet.currentHealth;
			h = pet.health;
		} else {
			this.healed = maxHealingValue;
		}
		pet.currentHealth = h;
		Action action = new Action() {
			
			@Override
			public boolean act(float delta) {
				callback.finished();
				return true;
			}
		};
		return action;
	}

	@Override
	public void used() {
		quantity -= 1;
	}

}
