package com.game.animalia.network;

import java.net.InetAddress;
import java.util.List;


public class NetworkData {
	
	public static final int LOBBY = 0;
	public static final int WAITING = 1;
	public static final int HOST_LIST = 2;
	public static final int SEARCHING_FOR_SERVER = 3;
	public static final int SHOW_PLAYERS_DATA = 4;
	public static final int BATTLE_SCREEN = 5;
	public static final int WAIT_FOR_TURN = 6;
	public static final int INTERPRET_TURN = 7;
	
	private static NetworkData instance;
	
	public int state;
	public List<InetAddress> hosts;
	public MultiplayerConnection connection;
	public boolean isServer;
	public boolean isClient;
	
	public static NetworkData getInstance() {
		if(instance == null) {
			instance = new NetworkData();
		}
		return instance;
	}
	
	

}
