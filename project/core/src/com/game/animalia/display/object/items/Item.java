package com.game.animalia.display.object.items;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.game.animalia.display.object.ActionFinishedCallback;
import com.game.animalia.display.object.pet.Pet;

public abstract class Item {
	
	public String name;
	public String desc;
	public int quantity;
	public int price;

	public Item(String name) {
		this.name = name;
		
	}
	
	public abstract String getText(Pet pet);
	public abstract Action getAction(Pet pet,ActionFinishedCallback callback);
	public abstract void used();
	

}
