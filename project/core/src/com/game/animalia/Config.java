package com.game.animalia;

public class Config {

	public static final float SCREEN_WIDTH = 800;
	public static final float SCREEN_HEIGHT = 480;
	
	public static final String PREFERENCE = ".animalandia";
	public static final float EXP_RATE = 5;//x5 exp rate
	
}
