package com.game.animalia.display.object.effects;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.game.animalia.Assets;
import com.game.animalia.display.object.TapCallback;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.animalia.display.screen.BattleScreen;
import com.game.animalia.network.NetworkData;
import com.game.framework.display.DisplayObject;
import com.game.framework.utils.L;


public class FearEffect extends Effect {

	public static final String NAME = "fear";
	private PetDisplay display;
	private float chanceToDispell = .5f;
	public DisplayObject iconDisplay;

	public FearEffect(PetDisplay display) {
		super(NAME);
		this.display = display;
		
		addActor(getDisplayObject());
	}
	
	public DisplayObject getDisplayObject() {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion fear = atlas.findRegion("fear");
		iconDisplay = new DisplayObject(fear);
		return iconDisplay;
	}

	@Override
	public void activateEffect(final BattleScreen screen,final EffectCallback callback) {
		float randomChance = (float)Math.random() + chanceToDispell;
		if(NetworkData.getInstance().isClient || NetworkData.getInstance().isServer) {
			int additionalSeed = 0;
			if(NetworkData.getInstance().isServer) {
				if(display == screen.playerPetDisplay) additionalSeed = 12;
				else additionalSeed = 13;
			}
			if(NetworkData.getInstance().isClient) {
				if(display == screen.playerPetDisplay) additionalSeed = 13;
				else additionalSeed = 12;
			}
			Random random = new Random((long)screen.randomSeed + additionalSeed);
			randomChance = (random.nextInt(100) * .01f) + chanceToDispell;
		}
		Pet pet = display.getPet();
		if(randomChance > 1) {
			remove();
			display.getEffects().remove(this);
			screen.showDialogue(pet.name+" is recovered from fear!", new TapCallback() {
				
				@Override
				public void tap() {
					callback.callback();
				}
			});
		} else {
			
			screen.showDialogue(pet.name+" is still affected by fear!", new TapCallback() {
				
				@Override
				public void tap() {
					callback.callback();
				}
			});
		}
	}
	
}
