package com.game.animalia.display.screen;

import java.util.ArrayList;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.game.animalia.Assets;
import com.game.animalia.util.Core;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;

public class MainMenuScreen extends AbstractScreen {

	private BitmapFont font;
	private DisplayText selected;

	public MainMenuScreen() {
		super();
		
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");

		font = Assets.getInstance().generateFont(20, Assets.getInstance().charSet);
		
		TextureRegion bg = atlas.findRegion("mainmenubg");
		DisplayObject bgDisplay = new DisplayObject(bg);
		addActor(bgDisplay);

		addMenuWindow();
		
		initScreen();
	}

	private void addMenuWindow() {
		DisplayObject menuWindow = createWindow("window6",84,88,200,200);
		addActor(menuWindow);
		
		final ArrayList<DisplayText> buttons = new ArrayList<DisplayText>();
		final DisplayText newGameText = createText("New Game",22,152);
		newGameText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				//if(selected==newGameText) {
					switchScreen(new NewGameScreen());
				//}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		final DisplayText continueText = createText("Continue",22,107);
		if(!Core.getInstance().hasSaved()) {
			continueText.setColor(Color.GRAY);
			continueText.setTouchable(Touchable.disabled);
		}
		continueText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				//if(selected==continueText){
					Core.getInstance().load();
					switchScreen(new HomeScreen());
				//}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		final DisplayText optionText = createText("Option",40,65);
		optionText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
//				if(selected==optionText){
//					
//				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		final DisplayText creditsText = createText("Credits",30,22);
		creditsText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
//				if(selected==creditsText) {
//					
//				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		buttons.add(newGameText);
		buttons.add(continueText);
		//buttons.add(optionText);
		//buttons.add(creditsText);
		for(int i=0;i<buttons.size();i++) {
			final DisplayText text = buttons.get(i);
			buttons.get(i).addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					selected = text;
					highLight(buttons, text);
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		}
		
		menuWindow.addActor(newGameText);
		menuWindow.addActor(continueText);
		//menuWindow.addActor(optionText);
		//menuWindow.addActor(creditsText);
	}
	
	private void highLight(ArrayList<DisplayText> list ,DisplayText text) {
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getTouchable() == Touchable.disabled) continue;
			list.get(i).setColor(Color.BLACK);
		}
		for(int i=0;i<list.size();i++) {
			if(text == list.get(i)) {
				list.get(i).setColor(Color.BLUE);
			}
		}
	}

	private DisplayText createText(String string, float x, float y) {

		DisplayText textDisplay = new DisplayText(string, font);
		textDisplay.setColor(Color.BLACK);
		textDisplay.setPosition(x, y);
		// textDisplay.isDebug = true;
//		 textDisplay.addListener(new ActorDragListener());
		return textDisplay;
	}

	private DisplayObject createWindow(String textureName, float x, float y,
			float width, float height) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		final NinePatch texture = atlas.createPatch(textureName);

		DisplayObject window = new DisplayObject() {
			@Override
			public void draw(Batch batch, float parentAlpha) {
				texture.draw(batch, getX(), getY(), getWidth(), getHeight());
				super.draw(batch, parentAlpha);
			}
		};
		window.setPosition(x, y);
		window.setSize(width, height);
//		window.addListener(new ActorDragListener());
		return window;
	}
}
