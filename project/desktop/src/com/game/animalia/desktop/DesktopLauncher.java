package com.game.animalia.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.game.animalia.AnimaliaGame;
import com.game.animalia.Config;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = (int)Config.SCREEN_WIDTH;
		config.height = (int)Config.SCREEN_HEIGHT;
		new LwjglApplication(new AnimaliaGame(), config);
	}
}
