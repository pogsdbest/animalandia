package com.game.animalia.display.screen;

import java.util.ArrayList;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.game.animalia.Assets;
import com.game.animalia.display.object.ActionFinishedCallback;
import com.game.animalia.display.object.pet.Pet;
import com.game.animalia.display.object.pet.PetDisplay;
import com.game.animalia.display.object.skills.PetSkill;
import com.game.animalia.util.Core;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.listeners.ActorDragListener;

public class AcademyScreen extends AbstractScreen {
	
	private BitmapFont font;
	private DisplayObject dialogueWindow;
	private DisplayObject listWindow;
	private DisplayObject statsWindow;
	private DisplayText selected;
	private DisplayText upgradeText;
	private PetSkill selectedSkill;
	private DisplayText skillDetailText;
	private PetDisplay petDisplay;
	private boolean actionIsFinished = true;
	private ArrayList<DisplayText> skillTextList;

	public AcademyScreen() {
		super();
		
		font = Assets.getInstance().generateFont(20, Assets.getInstance().charSet);
		Assets.getInstance().playMusic("sfx/home.mp3");
		
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion bg = atlas.findRegion("battlescreenbg");
		DisplayObject bgDisplay = new DisplayObject(bg);
		addActor(bgDisplay);
		
		addDialogueWindow();
		addMenuWindow();
		addSkillsWindow();
		addStatsWindow();
		
		showDialogue("hi! this is the academy what skill of your pet will you enhance?");
		initScreen();
	}
	
	private void addStatsWindow() {
		statsWindow = createWindow("window6", 420,184, 360, 260);
		addActor(statsWindow);
		
		showStats();
	}
	
	private void showStats() {
		String text = "Stats";
		DisplayText statsText = createText(text, 15, 0, font);
		statsText.setY(listWindow.getHeight() - 20 - statsText.getTextHeight());
		statsWindow.addActor(statsText);
		
		Pet pet = Core.getInstance().player.getPet();
		addPetDisplay();
		
		skillDetailText = createText("Level:\n" +
				"Damage:\n" +
				"Hitrate:\n" +
				"Speed:", 15, 0, font);
		skillDetailText.setY(15);
		skillDetailText.setPreferedWidth(statsWindow.getWidth() - 30);
		statsWindow.addActor(skillDetailText);
	}
	
	private void addPetDisplay() {
		Pet pet = Core.getInstance().player.getPet();
		
		petDisplay = new PetDisplay(pet);
		//petDisplay.setOrigin(petDisplay.getWidth()/2, petDisplay.getHeight()/2);
		petDisplay.setPosition(126,103);
		statsWindow.addActor(petDisplay);
//		petDisplay.addListener(new ActorDragListener());
		
	}

	private void addSkillsWindow() {
		listWindow = createWindow("window6", 20,184, 360, 260);
		addActor(listWindow);
		
		showPetSkills();
	}
	
	private void showPetSkills() {
		listWindow.clearChildren();
		Pet pet = Core.getInstance().player.getPet();
		String text = "Skills";
		DisplayText skillsText = createText(text, 15, 0, font);
		skillsText.setY(listWindow.getHeight() - 20 - skillsText.getTextHeight());
		listWindow.addActor(skillsText);
		
		final DisplayText goldText = createText("Gold", 15, 15, font);
		goldText.setPreferedWidth(listWindow.getWidth() - 30);
		goldText.addAction(new Action() {
			
			@Override
			public boolean act(float delta) {
				goldText.setText("Gold: "+Core.getInstance().player.getGold());
				return false;
			}
		});
		listWindow.addActor(goldText);
		
		ArrayList<PetSkill> list = pet.getSkills();
		skillTextList = new ArrayList<DisplayText>();
		
		for(int i =0;i<list.size();i++) {
			final DisplayText displayText = createText(">"+list.get(i).name, 0, 0, font);
			skillTextList.add(displayText);
			final PetSkill skill = list.get(i);
			displayText.addListener(new InputListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					highLight(skillTextList, displayText);
					
					selectedSkill = skill;
					showDialogue(skill.desc);
					enableUpgradeText();
					updateSkillDetailText();
					return super.touchDown(event, x, y, pointer, button);
				}
			});
		}
		createList(listWindow, skillTextList);
	}
	
	protected void updateSkillDetailText() {
		if(selectedSkill!=null) {
			Pet pet = Core.getInstance().player.getPet();
			skillDetailText.setText(
					"Level: "+selectedSkill.level+"\n"+
					"Damage: " +selectedSkill.getDamage(pet)+"\n"+
					"Hitrate: " +selectedSkill.getHitrate(pet)+"\n"+
					"Speed: "+selectedSkill.getSpeed(pet));
			if(actionIsFinished) {
				actionIsFinished = false;
				petDisplay.setTwenManager(manager);
				petDisplay.addAction(selectedSkill.getAction(petDisplay, new ActionFinishedCallback() {
					
					@Override
					public void finished() {
						actionIsFinished = true;
					}
				}));
			}
		}
	}

	private void highLight(ArrayList<DisplayText> list ,DisplayText text) {
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getTouchable() == Touchable.disabled) {
				list.get(i).setColor(Color.GRAY);
			} else {
				list.get(i).setColor(Color.BLACK);
			}
		}
		for(int i=0;i<list.size();i++) {
			if(text == list.get(i) && list.get(i).getTouchable()== Touchable.enabled) {
				list.get(i).setColor(Color.BLUE);
			}
		}
	}
	
	private void disableUpgradeText() {
		upgradeText.setTouchable(Touchable.disabled);
		upgradeText.setColor(Color.GRAY);
	}
	
	private void enableUpgradeText() {
		upgradeText.setTouchable(Touchable.enabled);
		upgradeText.setColor(Color.BLACK);
	}

	private void addMenuWindow() {
		DisplayObject menuWindow = createWindow("window6", 587,13, 200, 150);
		addActor(menuWindow);
		
		final ArrayList<DisplayText> list = new ArrayList<DisplayText>();
		upgradeText = createText("Upgrade", 26,95, font);
		disableUpgradeText();
		upgradeText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(selected == upgradeText){
					int playerGold = Core.getInstance().player.getGold();
					int price = (selectedSkill.level * 100);
					if(playerGold >= price){
						selectedSkill.level += 1;
						updateSkillDetailText();
						disableUpgradeText();
						showPetSkills();
						showDialogue(selectedSkill.name+" upgraded to level "+selectedSkill.level);
						selected = null;
						selectedSkill = null;
						Core.getInstance().player.setGold(playerGold - price);
					} else {
						disableUpgradeText();
						selected = null;
						showDialogue("you don't have enough gold.");
					}
				} else {
					selected = upgradeText;
					highLight(list, upgradeText);
					showDialogue("are you sure you want to upgrade "+selectedSkill.name+" for "+(selectedSkill.level * 100)+" gold?\n\n(Tap Upgrade again)");
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		final DisplayText exitText = createText("Exit", 26, 38, font);
		exitText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				//if(selected == exitText){
				selected = exitText;
				highLight(list, exitText);
				showDialogue("thank you! please come again.");
				
					switchScreen(new MapScreen());
				//} else {
					
				//}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		
		list.add(upgradeText);
		list.add(exitText);
		menuWindow.addActor(upgradeText);
		menuWindow.addActor(exitText);
	}

	private void addDialogueWindow() {
		dialogueWindow = createWindow("window6", 18,13, 560, 150);
		addActor(dialogueWindow);
	}
	
	private void showDialogue(String text) {
		dialogueWindow.clearChildren();
		
		DisplayText textDisplay = createText(text, 0, 0, font);
		textDisplay.setPreferedWidth(dialogueWindow.getWidth() - 40);
		textDisplay.setPosition(20, dialogueWindow.getHeight() - textDisplay.getTextHeight() - 25);
		dialogueWindow.addActor(textDisplay);
	}
	
	private DisplayObject createWindow(String textureName , float x, float y ,  float width , float height ) {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		final NinePatch texture = atlas.createPatch(textureName);
		
		DisplayObject window = new DisplayObject(){
			@Override
			public void draw(Batch batch, float parentAlpha) {
				texture.draw(batch, getX(), getY(), getWidth(), getHeight());
				super.draw(batch, parentAlpha);
			}
		};
		window.setPosition(x, y);
		window.setSize(width, height);
//		window.addListener(new ActorDragListener());
		return window;
	}
	
	private DisplayText createText(String string, float x, float y,
			BitmapFont font) {

		DisplayText textDisplay = new DisplayText(string, font);
		textDisplay.setColor(Color.BLACK);
		textDisplay.setPosition(x, y);
		// textDisplay.isDebug = true;
//		 textDisplay.addListener(new ActorDragListener());
		return textDisplay;
	}
	
	private void createList(DisplayObject listWindow ,ArrayList<DisplayText> list) {
		for(int i=0; i < list.size();i++) {
			DisplayText listItem = list.get(i);
			float x = 15;
			float y = listWindow.getHeight() - 80 - (i * 40);
			listItem.setPreferedWidth(listWindow.getWidth());
			listItem.setAlignment(HAlignment.LEFT);
			listItem.setPosition(x ,y);
			listWindow.addActor(listItem);
		}
		
	}

}
